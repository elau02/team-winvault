﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Net.Http;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json.Linq;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace LoginFeasibility
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e) {
           string userName = txtUserName.Text;
           string password = txtPassword.Password;

           //Post call to Vault Server
           using (var client = new HttpClient()) {
              client.BaseAddress = new Uri("https://calpoly-capstone3.veevavault.com/");
              var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("username", userName),
                new KeyValuePair<string, string>("password", password)
              });

              //send 
              var result = client.PostAsync("api/v7.0/auth", content).Result;

              //parse the result as json
              dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
              string statusText = "LOGIN IN FAILED.";

              //update our status text if the login succeeded
              //note that the attributes I'm accessing are dynamic
              string responseStatus = res.responseStatus;
              if (responseStatus.Equals("SUCCESS")) {
                 statusText = "LOG IN SUCCEEDED.\n" +
                    "userId = " + res.userId + "\n" +
                    "sessionId = " + res.sessionId + "\n";
              }
             
              lblStatus.Text = statusText;
           }

        }
    }
}
