﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Net.Http;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json.Linq;

/*
 * Almost identical to MainPage, but is navigated to.
 */
namespace VaultSwitchingFeasibility
{
    public sealed partial class SwitchVault : Page
    {
        public Uri vaultUri;

        public SwitchVault()
        {
            this.InitializeComponent();
        }

        //gets vault DNS as string from DocumentList
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            vaultUri = new Uri(e.Parameter as string);
        }

        private void btnSwitchLogin_Click(object sender, RoutedEventArgs e)
        {
            
            string userName = switchTxtUserName.Text;
            string password = switchTxtPassword.Password;

            //Post call to Vault Server
            using (var client = new HttpClient())
            {
                client.BaseAddress = vaultUri;
                var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("username", userName),
                new KeyValuePair<string, string>("password", password)
              });

                //send 
                var result = client.PostAsync("api/v7.0/auth", content).Result;

                //parse the result as json
                dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                string sessionId = (string)res.sessionId;
                string statusText = "LOGIN IN FAILED.";
                string uInfo = result.Content.ReadAsStringAsync().Result;

                //update our status text if the login succeeded
                //note that the attributes I'm accessing are dynamic
                string responseStatus = res.responseStatus;
                if (responseStatus.Equals("SUCCESS"))
                {
                    /*statusText = "LOG IN SUCCEEDED.\n" +
                       "userId = " + res.userId + "\n" +
                       "sessionId = " + res.responseStatus + "\n";
                    lblStatus.Text = statusText;*/

                    //Navigate to the DocumentList Screen, passing in the sessionId
                    this.Frame.Navigate(typeof(DocumentList), uInfo);
                }
                lblSwitchStatus.Text = statusText;
            }
        }

        private void Password_Key_Down(object sender, KeyRoutedEventArgs e)
        {
            //if(e.Key == VK_RETURN)
              //  this.btnLogin_Click(sender, e);
        }
    }
}
