﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Net.Http;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json.Linq;

namespace VaultSwitchingFeasibility
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            
            string userName = txtUserName.Text;
            string password = txtPassword.Password;

            //Post call to Vault Server
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://calpoly-capstone3.veevavault.com/");
                var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("username", userName),
                new KeyValuePair<string, string>("password", password)
              });

                //send 
                var result = client.PostAsync("api/v7.0/auth", content).Result;

                //parse the result as json
                dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                string sessionId = (string)res.sessionId;
                string statusText = "LOGIN IN FAILED.";
                string uInfo = result.Content.ReadAsStringAsync().Result;

                //update our status text if the login succeeded
                //note that the attributes I'm accessing are dynamic
                string responseStatus = res.responseStatus;
                if (responseStatus.Equals("SUCCESS"))
                {
                    /*statusText = "LOG IN SUCCEEDED.\n" +
                       "userId = " + res.userId + "\n" +
                       "sessionId = " + res.responseStatus + "\n";
                    lblStatus.Text = statusText;*/

                    //Navigate to the DocumentList Screen, passing in the sessionId
                    this.Frame.Navigate(typeof(DocumentList), uInfo);
                }
                lblStatus.Text = statusText;

            }
            

        }

        private void Password_Key_Down(object sender, KeyRoutedEventArgs e)
        {
            //if(e.Key == VK_RETURN)
              //  this.btnLogin_Click(sender, e);
        }
    }
}
