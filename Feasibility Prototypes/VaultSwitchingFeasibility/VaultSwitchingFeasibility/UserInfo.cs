﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace VaultSwitchingFeasibility
{
    class UserInfo
    {
        public string sessionId;
        public int userId;
        public int vaultId;
        public List<VaultInfo> vaults { get; set; }

        public UserInfo(String raw)
        {
            dynamic res = JObject.Parse(raw);
            vaults = res.vaultIds.ToObject<List<VaultInfo>>();
            
            this.sessionId = (string)res.sessionId;
            this.userId = (int)res.userId;
            this.vaultId = (int)res.vaultId;
        }

    }
}
