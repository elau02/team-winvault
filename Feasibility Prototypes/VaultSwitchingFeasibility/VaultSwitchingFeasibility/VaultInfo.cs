﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaultSwitchingFeasibility
{
    class VaultInfo
    {
        public int id { get; set; }
        public string name { get; set; }
        public Uri url { get; set; }

        public VaultInfo()
        {

        }
    }
}
