﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Net.Http;
using System.Net.Http.Headers;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Threading.Tasks;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace DocumentViewingFeasibility
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class DocumentList : Page
    {
        string sessionId; //current user sessionID
        int size; //number of documents in vault
        int limit = 5; //Limit for how many responses we get from an API call
        int start = 0; //Start number for the API call

        public DocumentList()
        {
            this.InitializeComponent();
        }

        private async void getDocuments()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://calpoly-capstone3.veevavault.com/");
                //create the Authroization header field with the sessionId received
                client.DefaultRequestHeaders.Add("Authorization", this.sessionId);

                //Initial GET Request to obtain the first 5 documents and to set the size
                //Wait for the GET Request to finish, then continue to parse the JSON file for size and first 5 documents
                HttpResponseMessage result = await client.GetAsync("api/v7.0/objects/documents/?" + "start=" + start + "&limit=" + limit);
                dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

                result.EnsureSuccessStatusCode();
                this.size = (int)res.size;
                addToList(res);

                //Continue GET request for the rest of the documents in the vault
                while (this.size > 0)
                {
                    //GET request for all documents and their properties
                    result = await client.GetAsync("api/v7.0/objects/documents/?" + "start=" + start + "&limit=" + limit);
                    result.EnsureSuccessStatusCode();
                    //Wait for the GET request to finish, then continue to update the GUI
                    //parse json results
                    res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    addToList(res);
                }
            }
        }

        private void addToList(dynamic res)
        {
            //Get the JArray of document JObjects
            JArray documentsList = (JArray)res.documents;
            //Go through every JObject
            foreach (JObject obj in documentsList)
            {
                //Get the inner JObect which contains a documents properties
                JObject documentObj = (JObject)obj.GetValue("document");
                listView1.Items.Add("ID: " + documentObj.GetValue("id") + "\nDocument Name: " + documentObj.GetValue("name__v")
                        + "\nStatus: " + documentObj.GetValue("status__v"));
            }
            this.size -= limit;
            this.start += limit;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //receive the passed in parameter from LoginPage
            this.sessionId = e.Parameter as string;
            //get documents from veeva vault api
            this.getDocuments();
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }


        private void MySelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // grab document id and pass it to DocumentViewer
            // Get item description string
            string docDescrip = (string)e.AddedItems[0];
            Boolean foundInt = false;

            // Get document ID from the string
            string resultID = "";
            for (int i = 0; i < docDescrip.Length; i++)
            {
                if (Char.IsDigit(docDescrip[i]))
                {
                    //check if the current char is digit
                    resultID += docDescrip[i];
                    foundInt = true;
                }
                else if (foundInt)
                {
                    break;
                }
            }
            string[] values = { resultID, this.sessionId };
            this.Frame.Navigate(typeof(DocumentViewer), values);

            // Feasibility for downloading a folder to your local file system
            // Called when a user selects an item in the document list
            /*
            //Debug.WriteLine("Selected: {0}", e.AddedItems[0]);

            // Get item description string
            string docDescrip = (string)e.AddedItems[0];
            Boolean foundInt = false;

            // Get document ID from the string
            string resultID = "";
            for (int i = 0; i < docDescrip.Length; i++)
            {
                if (Char.IsDigit(docDescrip[i]))
                {
                    //check if the current char is digit
                    resultID += docDescrip[i];
                    foundInt = true;
                }
                else if (foundInt)
                {
                        break;
                }
            }
            //Debug.WriteLine("got ID: " + resultID);

            using (var client = new HttpClient())
            {
                // See Downloading a File: http://msdn.microsoft.com/en-us/library/windows/apps/jj152726.aspx
                try
                {
                    client.BaseAddress = new Uri("https://calpoly-capstone3.veevavault.com/");
                    //create the Authroization header field with the sessionId received
                    client.DefaultRequestHeaders.Add("Authorization", this.sessionId);

                    // Increase the max buffer size for the response so we don't get an exception with so many web sites
                    client.MaxResponseContentBufferSize = 256000;

                    // Retrieve Document File veeva vault API call 
                    HttpResponseMessage response = await client.GetAsync("api/v7.0/objects/documents/" + resultID + "/file");

                    response.EnsureSuccessStatusCode();

                    // get document data from HttpResponseMessage.Content
                    byte[] data = await response.Content.ReadAsByteArrayAsync();

                    // Open up or create a folder in Downloads for Vault Documents
                    //StorageFolder newFolder = await DownloadsFolder.CreateFolderAsync("Vault Docs");

                    //Create the File
                    StorageFile myFile = await DownloadsFolder.CreateFileAsync("document" + resultID+".pdf");

                    //Write File data to the file
                    await FileIO.WriteBytesAsync(myFile, data);

                    // Go to your Downloads > DocumentListFeasibility folder to view!
                }
                catch (Exception ex)
                {

                }
            }
             */
        }


    }
}
