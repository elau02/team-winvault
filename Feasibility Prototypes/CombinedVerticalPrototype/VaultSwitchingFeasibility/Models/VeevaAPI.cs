﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace VaultSwitchingFeasibility.Models {

   class VeevaAPI {
      private static Uri _baseAddress;
      public static Uri BaseAddress {
         get { return _baseAddress; }
         set {
            _baseAddress = value;
            //client needs to be re-initalized when changing a property
            client = new HttpClient();
            client.BaseAddress = _baseAddress;
         }
      }
      private static HttpClient client;

      public VeevaAPI() {
         client = new HttpClient();
         client.MaxResponseContentBufferSize = 512000; //primarily for downloading docs
      }

      public static JObject Login(LoginModel user) {
         var content = new FormUrlEncodedContent(new[] {
           new KeyValuePair<string, string>("username", user.UserName),
           new KeyValuePair<string, string>("password", user.Password)
         });

         var result = client.PostAsync("api/v7.0/auth", content).Result;
         dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
         if (((String)res.responseStatus).Equals("SUCCESS"))
         {
            client.DefaultRequestHeaders.Add("Authorization", (string)res.sessionId);
            UserModel loggedInUser = UserModel.Instance;
            loggedInUser.SetUser(res);
         }
         return res;
      }

      public static Task<HttpResponseMessage> GetDocuments(int start, int limit) {
         return client.GetAsync("api/v7.0/objects/documents/?" + "start=" + start + "&limit=" + limit);
      }

      public static Task<HttpResponseMessage> DownloadDocument(string docId) {
         return client.GetAsync("api/v7.0/objects/documents/" + docId + "/file");
      }

      public static Task<HttpResponseMessage> GetDocumentMetadata(string docId) {
         return client.GetAsync("api/v7.0/objects/documents/" + docId);
      }
   }
}
