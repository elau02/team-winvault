﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace VaultSwitchingFeasibility {
   class UserModel {

      private static UserModel instance;
      public string SessionId { get; set; }
      public int UserId { get; set; }
      public int VaultId { get; set; }
      public List<VaultModel> Vaults { get; set; }

      private UserModel() { }

      public static UserModel Instance {
         get {
            if (instance == null)
               instance = new UserModel();
            return instance;
         }
      }

      public void SetUser(dynamic user) {
         Vaults = user.vaultIds.ToObject<List<VaultModel>>();
         SessionId = (string)user.sessionId;
         UserId = (int)user.userId;
         VaultId = (int)user.vaultId;
      }

   }
}
