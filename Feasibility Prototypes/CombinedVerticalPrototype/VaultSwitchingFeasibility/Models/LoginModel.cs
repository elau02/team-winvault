﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaultSwitchingFeasibility.Models {
   class LoginModel {

      public String UserName { get; set; }
      public String Password { get; set; }

      public LoginModel() { }

      public LoginModel(string username, string pw) {
         UserName = username;
         Password = pw;
      }
   }
}
