﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Net.Http;
using System.Net.Http.Headers;
using Windows.Data.Pdf;
using Windows.Storage;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using Newtonsoft.Json.Linq;
using VaultSwitchingFeasibility.Models;
using System.Diagnostics;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace VaultSwitchingFeasibility {
   /// <summary>
   /// A basic page that provides characteristics common to most applications.
   /// </summary>
   public sealed partial class DocumentViewer : Page {

      //private NavigationHelper navigationHelper;
      //private ObservableDictionary defaultViewModel = new ObservableDictionary();
      private string docId;
      private string sessionId;

      /// <summary>
      /// This can be changed to a strongly typed view model.
      /// </summary>
      /// 

      /*
      public ObservableDictionary DefaultViewModel
      {
          get { return this.defaultViewModel; }
      }

      /// <summary>
      /// NavigationHelper is used on each page to aid in navigation and 
      /// process lifetime management
      /// </summary>
      public NavigationHelper NavigationHelper
      {
          get { return this.navigationHelper; }
      }
      */

      public DocumentViewer() {
         this.InitializeComponent();
         // may need to comment these out
         //this.navigationHelper = new NavigationHelper(this);
         //this.navigationHelper.LoadState += navigationHelper_LoadState;
         //this.navigationHelper.SaveState += navigationHelper_SaveState;
      }

      /// <summary>
      /// Populates the page with content passed during navigation. Any saved state is also
      /// provided when recreating a page from a prior session.
      /// </summary>
      /// <param name="sender">
      /// The source of the event; typically <see cref="NavigationHelper"/>
      /// </param>
      /// <param name="e">Event data that provides both the navigation parameter passed to
      /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
      /// a dictionary of state preserved by this page during an earlier
      /// session. The state will be null the first time a page is visited.</param>
      //private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
      //{
      //}

      private async void btnDownload_Click(object sender, RoutedEventArgs e) {
         // See Downloading a File: http://msdn.microsoft.com/en-us/library/windows/apps/jj152726.aspx
         try {

            // Retrieve Document File veeva vault API call 
            HttpResponseMessage response = await VeevaAPI.DownloadDocument(docId);
            response.EnsureSuccessStatusCode();

            // get document data from HttpResponseMessage.Content
            byte[] data = await response.Content.ReadAsByteArrayAsync();

            // Open up or create a folder in Downloads for Vault Documents
            //StorageFolder newFolder = await DownloadsFolder.CreateFolderAsync("Vault Docs");

            //Create the File
            StorageFile myFile = await DownloadsFolder.CreateFileAsync("document" + docId + ".pdf");

            //Write File data to the file
            await FileIO.WriteBytesAsync(myFile, data);

            // Go to your Downloads > DocumentListFeasibility folder to view!
         }
         catch (Exception ex) {
            Debug.WriteLine(ex.StackTrace);
         }

      }

      private async void renderDocument() {
         HttpResponseMessage result = await VeevaAPI.DownloadDocument(docId);
         result.EnsureSuccessStatusCode();

         StorageFolder myfolder = Windows.Storage.ApplicationData.Current.LocalFolder;
         // don't use this folder - gives unauthorized access exception
         //StorageFolder myfolder = Windows.ApplicationModel.Package.Current.InstalledLocation; 
         StorageFile sampleFile = await myfolder.CreateFileAsync(docId, CreationCollisionOption.ReplaceExisting);
         byte[] file = await result.Content.ReadAsByteArrayAsync();


         await FileIO.WriteBytesAsync(sampleFile, file); // creates pdf file that was retrieved

         RenderPDF(myfolder, docId);

         // for some reason, you can only open a file once. if you try to open it again, the program throws 
         // an exception with an "unspecified error." 
      }

      private async void RenderPDF(StorageFolder folder, string filename) {
         try {
            StorageFile doc = await folder.GetFileAsync(docId);
            PdfDocument pdf = await PdfDocument.LoadFromFileAsync(doc); // this line throws the exception

            for (uint ndx = 0; ndx < pdf.PageCount; ndx++) {
               var pdfPage = pdf.GetPage(ndx);

               if (pdfPage != null) {
                  StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;

                  StorageFile jpgFile = await tempFolder.CreateFileAsync(Guid.NewGuid().ToString() +
                      ".png", CreationCollisionOption.ReplaceExisting);
                  if (jpgFile != null) {
                     IRandomAccessStream randomStream = await jpgFile.OpenAsync(FileAccessMode.ReadWrite);

                     PdfPageRenderOptions pdfPageRenderOptions = new PdfPageRenderOptions();
                     //Render Pdf page with default options 
                     await pdfPage.RenderToStreamAsync(randomStream);

                     await randomStream.FlushAsync();

                     // free resources
                     randomStream.Dispose();
                     pdfPage.Dispose();

                     // create UI image
                     Image image = new Image();
                     BitmapImage temp = new BitmapImage();
                     temp.SetSource(await jpgFile.OpenAsync(FileAccessMode.Read));
                     image.Source = temp;
                     image.Stretch = Stretch.None;
                     image.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
                     documentView.Items.Add(image);
                  }
               }
            }
         }
         catch (Exception ex) {
            // 
         }
      }

      private async void getProperties() {
         HttpResponseMessage result = await VeevaAPI.GetDocumentMetadata(docId);
         result.EnsureSuccessStatusCode();
         dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

         JObject json = (JObject)res;
         JObject obj = (JObject)json.GetValue("document");
         JToken name = obj.GetValue("name__v");
         docName.Text = name.ToString();
         docNumber.Text = "Document Number: " + obj.GetValue("document_number__v").ToString();
         docCreationDate.Text = "Creation Date: " + obj.GetValue("document_creation_date__v").ToString();
         docStatus.Text = "Document Status: " + obj.GetValue("status__v").ToString();
         docType.Text = "Document Type: " + obj.GetValue("type__v").ToString();
         docSize.Text = "Document Size: " + obj.GetValue("size__v").ToString() + " bytes";

      }

      /// <summary>
      /// Preserves state associated with this page in case the application is suspended or the
      /// page is discarded from the navigation cache.  Values must conform to the serialization
      /// requirements of <see cref="SuspensionManager.SessionState"/>.
      /// </summary>
      /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
      /// <param name="e">Event data that provides an empty dictionary to be populated with
      /// serializable state.</param>
      /// 
      /*
      private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
      {
      } */

      private void BackClick(object sender, RoutedEventArgs e) {
         this.Frame.Navigate(typeof(DocumentList), sessionId); // go back to Documents List
      }

      #region NavigationHelper registration

      /// The methods provided in this section are simply used to allow
      /// NavigationHelper to respond to the page's navigation methods.
      /// 
      /// Page specific logic should be placed in event handlers for the  
      /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
      /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
      /// The navigation parameter is available in the LoadState method 
      /// in addition to page state preserved during an earlier session.

      protected override void OnNavigatedTo(NavigationEventArgs e) {
         //navigationHelper.OnNavigatedTo(e);
         string[] values = (string[])e.Parameter;
         docId = values[0];
         sessionId = values[1];
         this.getProperties();
         this.renderDocument();
      }

      protected override void OnNavigatedFrom(NavigationEventArgs e) {
         //navigationHelper.OnNavigatedFrom(e);
      }

      #endregion
   }
}
