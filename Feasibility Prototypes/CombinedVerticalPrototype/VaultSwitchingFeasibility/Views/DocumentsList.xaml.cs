﻿using VaultSwitchingFeasibility.Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Net.Http;
using System.Net.Http.Headers;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage.Pickers;
using Windows.Storage;
using System.Diagnostics;
using Windows.Storage.Streams;
using VaultSwitchingFeasibility.Models;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace VaultSwitchingFeasibility {
   /// <summary>
   /// A basic page that provides characteristics common to most applications.
   /// </summary>
   public sealed partial class DocumentList : Page {
      int size; //number of documents in vault
      int limit = 5; //Limit for how many responses we get from an API call
      int start = 0; //Start number for the API call
      UserModel user;

      public DocumentList() {
         this.InitializeComponent();
      }

      protected override void OnNavigatedTo(NavigationEventArgs e) {
         user = UserModel.Instance;
         MenuFlyoutItem holder;

         foreach (VaultModel v in UserModel.Instance.Vaults) {
            holder = new MenuFlyoutItem();
            holder.Text = v.name;
            holder.Click += flyout_Click;
            vaultList.Items.Add(holder);
         }

         //get documents from veeva vault api
         this.getDocuments();
      }

      private async void getDocuments() {
         Uri vaultSite = null;
         this.start = 0;

         foreach (VaultModel i in user.Vaults) {
            Debug.WriteLine("got", i.url);
            if (i.id == user.VaultId) {
               vaultSite = i.url;
               break;
            }
         }



         //Initial GET Request to obtain the first 5 documents and to set the size
         //Wait for the GET Request to finish, then continue to parse the JSON file for size and first 5 documents
         HttpResponseMessage result = await VeevaAPI.GetDocuments(size, limit);
         dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
         result.EnsureSuccessStatusCode();
         this.size = (int)res.size;
         addToList(res);

         //Continue GET request for the rest of the documents in the vault
         while (this.size > 0) {
            //GET request for all documents and their properties
            result = await VeevaAPI.GetDocuments(size, limit);
            result.EnsureSuccessStatusCode();
            //Wait for the GET request to finish, then continue to update the GUI
            //parse json results
            res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            addToList(res);
         }
      }

      private void addToList(dynamic res) {
         //Get the JArray of document JObjects
         JArray documentsList = (JArray)res.documents;
         //Go through every JObject
         foreach (JObject obj in documentsList) {
            //Get the inner JObect which contains a documents properties
            JObject documentObj = (JObject)obj.GetValue("document");
            listView1.Items.Add("ID: " + documentObj.GetValue("id") + "\nDocument Name: " + documentObj.GetValue("name__v")
                    + "\nStatus: " + documentObj.GetValue("status__v"));
         }
         this.size -= limit;
         this.start += limit;
      }



      private void OnClick(object sender, RoutedEventArgs e) {
         this.Frame.Navigate(typeof(Login));
      }

      // Feasibility for downloading a folder to your local file system
      // Called when a user selects an item in the document list
      private async void MySelectionChanged(object sender, SelectionChangedEventArgs e) {
         //Debug.WriteLine("Selected: {0}", e.AddedItems[0]);

         // Get item description string
         string docDescrip = (string)e.AddedItems[0];
         Boolean foundInt = false;

         // Get document ID from the string
         string resultID = "";
         for (int i = 0; i < docDescrip.Length; i++) {
            if (Char.IsDigit(docDescrip[i])) {
               //check if the current char is digit
               resultID += docDescrip[i];
               foundInt = true;
            }
            else if (foundInt) {
               break;
            }
         }

         string[] values = { resultID, user.SessionId };
         this.Frame.Navigate(typeof(DocumentViewer), values);
      }

      private void flyout_Click(object sender, object e) {
         MenuFlyoutItem item = sender as MenuFlyoutItem;
         string toPass = "not set";

         if (item != null) {
            string name = item.Text;

            foreach (VaultModel i in UserModel.Instance.Vaults) {
               if (i.name == name) {
                  user.VaultId = i.id;
                  toPass = i.url.OriginalString;
                  break;
               }
            }
            listView1.Items.Clear();
            this.Frame.Navigate(typeof(Login), toPass);
         }
      }
   }
}
