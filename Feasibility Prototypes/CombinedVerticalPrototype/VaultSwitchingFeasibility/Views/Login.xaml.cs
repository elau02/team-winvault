﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Net.Http;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Newtonsoft.Json.Linq;
using VaultSwitchingFeasibility.Models;

namespace VaultSwitchingFeasibility {
   /// <summary>
   /// An empty page that can be used on its own or navigated to within a Frame.
   /// </summary>
   public sealed partial class Login : Page {
      private Uri vaultUri;

      public Login() {
         this.InitializeComponent();
         vaultUri = new Uri("https://calpoly-capstone3.veevavault.com/");
      }

      //gets vault DNS as string from DocumentList
      protected override void OnNavigatedTo(NavigationEventArgs e) {

         vaultUri = e.Parameter == null ? vaultUri : new Uri(e.Parameter as string);
      }

      private void btnLogin_Click(object sender, RoutedEventArgs e) {
         LoginModel user = new LoginModel(txtUserName.Text, txtPassword.Password);
         VeevaAPI.BaseAddress = vaultUri;
         dynamic res = VeevaAPI.Login(user);

         string sessionId = (string)res.sessionId;
         string statusText = "LOGIN IN FAILED.";

         //update our status text if the login succeeded
         //note that the attributes I'm accessing are dynamic
         string responseStatus = res.responseStatus;
         if (responseStatus.Equals("SUCCESS")) {
            //Navigate to the DocumentList Screen
            this.Frame.Navigate(typeof(DocumentList));
         }
         lblStatus.Text = statusText;
      }
   }
}
