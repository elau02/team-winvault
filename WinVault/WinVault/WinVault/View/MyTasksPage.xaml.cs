﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using WinVault.ViewModel;
using WinVault.Model;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Ioc;
using WinVault.Common;
using System;
using System.Diagnostics;

namespace WinVault {
   public sealed partial class MyTasksPage {
      public MyTasksPage() {
         InitializeComponent();
      }

      public MyTasksViewModel ViewModel {
         get { return (MyTasksViewModel)DataContext; }
      }

      private void StopAnimatingProgressBar() {
         progressBar.IsIndeterminate = false;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }
      protected override void OnNavigatedTo(NavigationEventArgs e) {
         base.OnNavigatedTo(e);
         ViewModel.MTProgressBar = StopAnimatingProgressBar;

         progressBar.IsIndeterminate = true;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;

         //tell the viewmodel to get the document list
         var list = ViewModel.getMyTasksList(StopAnimatingProgressBar);
         //update the item source to be the return from the viewmodel
         this.myTasksGridView.ItemsSource = list;
      }

      protected override void OnNavigatedFrom(NavigationEventArgs e) {
         base.OnNavigatedFrom(e);
      }

      private void Button_Click(object sender, RoutedEventArgs e)
      {
          //SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(LibraryPage), arg);
      }

      private void MyTaskSelectionChanged(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e)
      {
          //get the MyTask that the user selected
          MyTask myTask = (MyTask)e.AddedItems[0];
          //navigate to the MyTaskPage, pass in the MyTask ID
          SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.View.MyTasksDetailPage), myTask);
      }
   }
}