﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using WinVault.ViewModel;
using WinVault.Model;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Ioc;
using WinVault.Common;
using System;
using Windows.UI.Xaml.Controls;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Media;
using Windows.UI;
using System.Collections.Generic;
using Windows.UI.Popups;
using Windows.Storage;
using System.Threading.Tasks;

namespace WinVault.View {
   public sealed partial class DocumentPage {
      private Grid currentVisibleGrid;
      private ObservableCollection<string> versionsList;
      private string docId;
      private List<Document> supDocs;

      public DocumentPage() {
         InitializeComponent();
         currentVisibleGrid = currentVisibleGrid == null ? gridGeneralProperties : currentVisibleGrid;
      }

      public DocumentViewModel ViewModel {
         get {
            return (DocumentViewModel)DataContext;
         }
      }

      private void StartAnimatingProgressBar() {
         progressBar.IsIndeterminate = true;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
      }

      private void StopAnimatingProgressBar() {
         progressBar.IsIndeterminate = false;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      protected override void OnNavigatedTo(NavigationEventArgs e) {
         base.OnNavigatedTo(e);
         StartAnimatingProgressBar();
         string[] strArray = (string[])e.Parameter;
         docId = strArray[0];
         string type = strArray[1];
         //length of 1 = document case
         if (type == "document") {
            //call async method to render document
            this.renderDocument(docId, null, null);
         }
         else if (type == "video") {
            //render the video
            this.renderVideo(docId, null, null);
         }

         ViewModel.SetId(docId);
      }

      protected override void OnNavigatedFrom(NavigationEventArgs e) {
         base.OnNavigatedFrom(e);
      }

      private async void renderVideo(string docID, string major, string minor) {
         StorageFile videoFile = await ViewModel.getVideo(docID, major, minor);
         
         if (videoFile != null) {
            //read the stream from the local storage file
            var stream = await videoFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
            //set the source to be the stream
            this.videoMediaElement.SetSource(stream, videoFile.ContentType);
            this.videoMediaElement.Width = Window.Current.CoreWindow.Bounds.Width - 780;
            //show the media element
            this.videoMediaElement.Visibility = Windows.UI.Xaml.Visibility.Visible;
            //hide the document view
            this.documentView.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
         }
         else {
            var msg = new MessageDialog("Could not open video.");
            msg.Title = "Error";
            msg.ShowAsync();
         }
         StopAnimatingProgressBar();
      }
      
      private async void renderDocument(string docID, string major, string minor) {
         //tell the viewmodel to get the document that has the ID in docID
         ObservableCollection<Image> docPDF = await ViewModel.getDocument(docID, major, minor);
         //if docPDF is not null then we succesfully received the document from Veeva's API
         if (docPDF != null) {
            foreach (Image page in docPDF) {
               // subtract properties from doc width
               page.Width = Window.Current.CoreWindow.Bounds.Width - 780;
               page.Stretch = Stretch.Uniform;
               this.documentView.Items.Add(page);
            }
         }
         else {
            var msg = new MessageDialog("Could not open document.");
            msg.Title = "Error";
            msg.ShowAsync();
         }
         StopAnimatingProgressBar();
      }

      private async void OnDownloadDocumentClick(object sender, RoutedEventArgs e) {
         byte[] data = await ViewModel.downloadDocument();
         if (data == null) {
            var msg = new MessageDialog("Could not download document.");
            msg.Title = "Error";
            msg.ShowAsync();
         }
         else {
            var msg = new MessageDialog("Document downloaded successfully!");
            msg.Title = "Success";
            msg.ShowAsync();
         }

      }

      private async void listMainProperties_SelectionChanged(object sender, SelectionChangedEventArgs e) {
         // Deselected, so make pdf full view
         if (((ListView)sender).SelectedItems.Count <= 0) {
            // Hide any and all properties grids
            collapseAllGridProperties();

            // display pdf semi-full screen
            docGrid.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            // Set margin to be next to side bar but below title bar
            docGrid.Margin = new Thickness(270, 0, 0, 0);
         }
         else if (currentVisibleGrid != null) {

            currentVisibleGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            String text = (String)((ListViewItem)((ListView)sender).SelectedItem).Content;
            Grid grid = (Grid)Root.FindName("grid" + text.Replace(" ", string.Empty));
            if (text.Equals("Versions")) {
               versionsList = await ViewModel.GetDocumentVersions(docId);
               versionsListView.ItemsSource = versionsList;
            }
            else if (text.Contains("Supporting")) {
               string version = ViewModel.Document.Version;
               string[] numbers = version.Split('.');
               supDocs = await ViewModel.GetSupportingDocuments(this.docId, numbers[0], numbers[1]);
               List<string> names = new List<string>();
               foreach (Document doc in supDocs) {
                  names.Add(doc.DocumentName);
               }
               supDocsListView.ItemsSource = names;
            }

            grid.Visibility = Windows.UI.Xaml.Visibility.Visible;
            currentVisibleGrid = grid;

            docGrid.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
            docGrid.Margin = new Thickness(0, 0, 0, 0);
         }
      }

      private async Task<List<string>> processShareSetting(ShareSetting setting) {

         List<string> users = await processUsers(setting.Users);
         List<string> groups = await processGroups(setting.Groups);

         users.AddRange(groups);
         return users;
      }

      private async Task<List<string>> processUsers(List<string> users) {

         List<string> usernames = new List<string>();
         foreach (string user in users) {
            string name = await ViewModel.GetUser(user);
            System.Diagnostics.Debug.WriteLine("Username: " + name);
            usernames.Add(name);
         }
         return usernames;
      }

      private async Task<List<string>> processGroups(List<string> groups) {
         List<string> groupnames = new List<string>();
         foreach (string group in groups) {
            string name = await ViewModel.GetGroupName(group);
            System.Diagnostics.Debug.WriteLine("GroupName: " + name);
            groupnames.Add(name);
         }
         return groupnames;
      }

      private void AppBarButtonEditField_Click(object sender, RoutedEventArgs e) {
         // Make save button enabled
         SaveButton.IsEnabled = true;
         SaveButton2.IsEnabled = true;

         //get parent of Edit button
         Grid parent = (Grid)(((AppBarButton)sender).Parent);

         //find the textbox by looking at the previous item
         int ndx = parent.Children.IndexOf((UIElement)sender);
         TextBox text = ((TextBox)(parent.Children[ndx - 1]));

         //set properties for the textbox
         text.IsReadOnly = false;
         text.Focus(FocusState.Keyboard);
      }

      private void btnSave_Click(object sender, RoutedEventArgs e) {
         foreach (UIElement item in currentVisibleGrid.Children) {
            if (!(item is AppBarButton))
               continue;

            int ndx = currentVisibleGrid.Children.IndexOf((UIElement)item);

            // only do it if ndx > 0; close pane button is an AppBarButton
            if (ndx > 0 && currentVisibleGrid.Children[ndx - 1] is TextBox) {
                  TextBox text = ((TextBox)(currentVisibleGrid.Children[ndx - 1]));
                  if (!text.IsReadOnly) {
                     VeevaAPI.UpdateDocumentProperty(ViewModel.Document.ID, text.Name, text.Text);
                     var msg = new MessageDialog("Saved changes to document metadata!");
                     msg.Title = "Success";
                     msg.ShowAsync();
                     text.IsReadOnly = true;
                  }
            }
         }

         //set the title 
         PageTitle.Text = name__v.Text;

         SaveButton.IsEnabled = false;
         SaveButton2.IsEnabled = false;
      }

      private async void versionsListView_SelectionChanged(object sender, SelectionChangedEventArgs e) {
        
         ListView view = (ListView)sender;
         if (view.SelectedItem != null) {
            string version = view.SelectedItem.ToString();

            string[] numbers = version.Split('.');

            documentView.Items.Clear();
            StartAnimatingProgressBar();
            Document versionDoc = await VeevaAPI.GetDocumentVersionProperties(docId, numbers[0], numbers[1]);
            if (versionDoc != null) {
               if (versionDoc.Format.Contains("video")) {
                  this.renderVideo(docId, numbers[0], numbers[1]);
               }
               else {
                  this.renderDocument(docId, numbers[0], numbers[1]);
               }
            }
            else {
               var msg = new MessageDialog("Failed to load versions. Check your internet connection.");
               msg.Title = "Failed to Get Versions";
               msg.ShowAsync();
            }
         }
      }

      private async void supDocsListView_SelectionChanged(object sender, SelectionChangedEventArgs e) {

         ListView view = (ListView)sender;
         if (view.SelectedItem != null) {
            string docName = view.SelectedItem.ToString();
            // search for doc name in list, get id, load support doc in view
            Document temp = null;
            foreach (Document doc in supDocs) {
               if (doc.DocumentName.Equals(docName)) {
                  temp = doc;
                  break;
               }
            }
            documentView.Items.Clear();
            StartAnimatingProgressBar();
            if (temp.Format.Contains("video")) {
               this.renderVideo(temp.ID, null, null);
            }
            else {
               this.renderDocument(temp.ID, null, null);
            }
         }
      }

      private void ClosePaneIcon_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e) {
         listMainProperties.SelectedIndex = -1;
         // Hide any and all properties grids
         collapseAllGridProperties();

         // display pdf semi-full screen
         docGrid.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
         // Set margin to be next to side bar but below title bar
         docGrid.Margin = new Thickness(270, 0, 0, 0);
      }

      private void collapseAllGridProperties() {
         gridGeneralProperties.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
         gridProductInformation.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
         gridSharingSettings.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
         gridSupportingDocuments.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
         gridVersions.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      private void ClosePaneButton_Click(object sender, RoutedEventArgs e) {
         listMainProperties.SelectedIndex = -1;
         // Hide any and all properties grids
         collapseAllGridProperties();

         // display pdf semi-full screen
         docGrid.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
         // Set margin to be next to side bar but below title bar
         docGrid.Margin = new Thickness(270, 0, 0, 0);
      }

      private async void ShowActiveWorkflow(object sender, RoutedEventArgs e) {
         ActiveWorkflow activeWorkflow = await ViewModel.getActiveWorkflow(docId);
         if (activeWorkflow == null) {
            var msg = new MessageDialog("There is no active workflow associated with this document.");
            msg.Title = "No Active Workflow";
            msg.ShowAsync();
         }
         else {
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.View.ActiveWorkflowsDetailPage), activeWorkflow);
         }
      }
   }
}
