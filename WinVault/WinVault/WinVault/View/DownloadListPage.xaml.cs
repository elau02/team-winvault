﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using WinVault.ViewModel;
using WinVault.Model;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Ioc;
using WinVault.Common;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.Storage;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238
namespace WinVault.View
{
    public sealed partial class DownloadListPage
    {
        public DownloadListPage()
        {
            InitializeComponent();
        }

        public DownloadListViewModel ViewModel
        {
            get
            {
                return (DownloadListViewModel)DataContext;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            getDownloadList();
        }

        //Handles telling the viewmodel to get the list of documents that are inside downloads folder
        private async void getDownloadList()
        {
           if (ViewModel != null) {
              var docList = await ViewModel.getDownloadList();
              //update the item source to be the return from the viewmodel
              if (docList != null) {
                 this.downloadListGridView.ItemsSource = docList;
              }
              else {
                 var msg = new MessageDialog("Could not open document list. The DownloadedDocuments folder may have been removed.");
                 msg.Title = "Error";
                 msg.ShowAsync();
              }
           }
        }

        private void DocumentSelectionChanged(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e) {
           //selection changed gets called twice, first during the OnNavigatedTo setting of the ItemsSource
           //second time during the actual selection(> -1 case)
           if (this.downloadListGridView.SelectedIndex > -1) {
              Document doc = (Document)e.AddedItems[0];
              //all videos downloaded are stored as mp4s in this application
              if (doc.DocumentType.Contains(".mp4")) {
                 renderDownloadedVideo(doc.DocumentName);
              }
              //all images downloaded are stored as pngs in this application
              else if (doc.DocumentType.Contains(".png")) {
                 //hide the video screen
                 this.videoMediaElement.Width = Window.Current.CoreWindow.Bounds.Width - 780;
                 this.videoMediaElement.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                 //make the document view visible
                 this.documentView.Visibility = Windows.UI.Xaml.Visibility.Visible;
                 renderDownloadedImage(doc.DocumentName);
              }
              //all documents downloaded are stored as pdfs in this application
              else {
                 //hide the video screen
                 this.videoMediaElement.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                 //make the document view visible
                 this.documentView.Visibility = Windows.UI.Xaml.Visibility.Visible;
                 renderDownloadedDocument(doc.DocumentName);
              }
           }
        }

        private async void renderDownloadedImage(string fileName) {
           //tell the viewmodel to get the document that has the ID in docID
           Image image = await ViewModel.getDownloadedImage(fileName);

           if (image != null) {
              this.documentView.Items.Clear();
              image.Height = 500; //without set heights and widths, image takes up the whole screen
              image.Width = Window.Current.CoreWindow.Bounds.Width - 780;
              this.documentView.Items.Add(image);
           }
           else {
              var msg = new MessageDialog("Could not open document.");
              msg.Title = "Error";
              msg.ShowAsync();
           }
        }

        private async void renderDownloadedVideo(string fileName) {
           StorageFile videoFile = await ViewModel.getDownloadedVideo(fileName);

           if (videoFile != null) {
              //read the stream from the local storage file
              var stream = await videoFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
              //set the source to be the stream
              this.videoMediaElement.SetSource(stream, videoFile.ContentType);
              //show the media element
              this.videoMediaElement.Visibility = Windows.UI.Xaml.Visibility.Visible;
              //hide the document view
              this.documentView.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
           }
           else {
              var msg = new MessageDialog("Could not open video.");
              msg.Title = "Error";
              msg.ShowAsync();
           }
        }

        private async void renderDownloadedDocument(string fileName) {
            //tell the viewmodel to get the document that has the ID in docID
           ObservableCollection<Image> docPDF = await ViewModel.getDownloadedDocument(fileName);
           //if docPDF is not null then we succesfully received the document from Veeva's API
           if (docPDF != null) {
              this.documentView.Items.Clear();
              foreach(Image image in docPDF)
                 this.documentView.Items.Add(image);
           }
           else {
              var msg = new MessageDialog("Could not open document.");
              msg.Title = "Error";
              msg.ShowAsync();
           }
        }
    }
}
