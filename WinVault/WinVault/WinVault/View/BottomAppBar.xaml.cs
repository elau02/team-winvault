﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinVault.Common;
using WinVault.Model;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236
namespace WinVault.View {
   public sealed partial class BottomAppBar : UserControl {

      private StreamContent streamContent;

      public BottomAppBar() {
         this.InitializeComponent();

         foreach (Vault v in User.Instance.Vaults) {
            MenuFlyoutItem vault = new MenuFlyoutItem();
            vault.Text = v.Name;
            vault.Click += switchVault;
            flyoutVault.Items.Add(vault);
         }
      }

      private void switchVault(object sender, object e) {
         MenuFlyoutItem item = sender as MenuFlyoutItem;
         VeevaAPI.BaseAddress = new Uri(User.Instance.Vaults.Find(c => c.Name.Equals(item.Text)).Url.OriginalString + "/" + VeevaAPI.version + "/");
         SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(LoginPage));
      }

      private void UserLogout_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e) {
         //First we need to find out how big our window is, so we can center to it.
         CoreWindow currentWindow = Window.Current.CoreWindow;

         //Set our background rectangle to fill the entire window
         rectBackgroundHide.Height = currentWindow.Bounds.Height;
         rectBackgroundHide.Width = currentWindow.Bounds.Width;
         rectBackgroundHide.Margin = new Thickness(0, 0, 0, 0);

         //Make sure the background is visible
         rectBackgroundHide.Visibility = Windows.UI.Xaml.Visibility.Visible;

         //Now we figure out where the center of the screen is, and we 
         //move the popup to that location.
         int horizontalSize = 600;
         int verticalSize = 400;
         popupLogout.HorizontalOffset = (currentWindow.Bounds.Width / 2) - (horizontalSize / 2);
         popupLogout.VerticalOffset = (currentWindow.Bounds.Height / 2) - (verticalSize / 2);
         popupLogout.IsOpen = true;
      }

      private void UserProfile_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e) {
      }

      private async void UploadDocument_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e) {
         cmboDocumentType.Items.Clear();
         foreach (String type in VeevaAPI.GetDocumentTypes())
            cmboDocumentType.Items.Add(type);

         cmboDocumentProduct.Items.Clear();
         foreach (String type in VeevaAPI.GetProductTypes())
            cmboDocumentProduct.Items.Add(type);

         var filePicker = new FileOpenPicker();
         filePicker.FileTypeFilter.Add("*");
         filePicker.ViewMode = PickerViewMode.List;
         filePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
         filePicker.SettingsIdentifier = "PicturePicker";
         var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
         //filePicker.CommitButtonText = "Select File";
         filePicker.CommitButtonText = loader.GetString("SelectFileButton");

         StorageFile file = await filePicker.PickSingleFileAsync();
         if (file != null) {
            var bytes = await GetBtyeFromFile(file);
            var props = await file.GetBasicPropertiesAsync();
            var stream = await file.OpenStreamForReadAsync();
            streamContent = new StreamContent(stream, (int)props.Size);

            txtDocumentName.Text = file.Name;
            //First we need to find out how big our window is, so we can center to it.
            CoreWindow currentWindow = Window.Current.CoreWindow;

            //Now we figure out where the center of the screen is, and we 
            //move the popup to that location.   
            int horizontalSize = 2000;
            popupUploadDocument.HorizontalOffset = (currentWindow.Bounds.Width / 2) - (horizontalSize / 2);
            popupUploadDocument.IsOpen = true;
               
         }
      }

      // This is the method to convert the StorageFile to a Byte[]          
      private async Task<byte[]> GetBtyeFromFile(StorageFile storageFile) {
         var stream = await storageFile.OpenReadAsync();

         using (var dataReader = new DataReader(stream)) {
            var bytes = new byte[stream.Size];
            await dataReader.LoadAsync((uint)stream.Size);
            dataReader.ReadBytes(bytes);

            return bytes;
         }
      }

      private void PopupClosePanel_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e) {
         //Close it all down
         rectBackgroundHide.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
         popupLogout.IsOpen = false;
         popupUploadDocument.IsOpen = false;
      }

      private void PopupUpload_Click(object sender, RoutedEventArgs e) {
         string validation = "";
         if (String.IsNullOrEmpty(txtDocumentName.Text))
            validation += "Document name is required\n";
         if (String.IsNullOrEmpty(txtDocumentVersion.Text))
            validation += "Document version is required\n";
         if (String.IsNullOrEmpty(txtDocumentTitle.Text))
            validation += "Document title is required\n";
         if (cmboDocumentType.SelectedValue == null)
            validation += "Document type is required\n";
         if (cmboDocumentProduct.SelectedValue == null)
            validation += "Document product is required\n";

         if (validation != "") {
            var msg = new MessageDialog("Please enter a value for the following fields: \n" + validation);
            msg.Title = "Document Upload Failed!";
            msg.ShowAsync();
         }
         else {
            var content = new MultipartFormDataContent();
            content.Add(new StringContent(txtDocumentName.Text), "name__v");
            content.Add(new StringContent(txtDocumentVersion.Text.Split('.')[0]), "major_version_number__v");
            content.Add(new StringContent(txtDocumentVersion.Text.Split('.')[1]), "minor_version_number__v");
            content.Add(new StringContent(cmboDocumentType.SelectedValue.ToString()), "type__v");
            content.Add(streamContent, "file", txtDocumentName.Text);
            content.Add(new StringContent("General Lifecycle"), "lifecycle__v");
            content.Add(new StringContent("true"), "required_boolean__c");
            content.Add(new StringContent(VeevaAPI.GetProductId(cmboDocumentProduct.SelectedValue.ToString())), "product__v");

            PopupClosePanel_Click(null, null);

            if (!VeevaAPI.UploadDocument(content)) {
               var msg = new MessageDialog("An error has occurred while uploading your document.");
               msg.Title = "Document Upload Failed!";
               msg.ShowAsync();
            }
            else {
               var msg = new MessageDialog("Document has been uploaded!");
               msg.Title = "Document Uploaded!";
               msg.ShowAsync();
            }
         }

      }

      private void PopupLogoutYes_Click(object sender, RoutedEventArgs e) {
         VeevaAPI.Logout();
         SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(LoginPage));
      }

      private void DownloadList_Click(object sender, RoutedEventArgs e)
      {
          SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(DownloadListPage));
      }
   }
}
