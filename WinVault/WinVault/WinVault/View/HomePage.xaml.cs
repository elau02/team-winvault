﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Navigation;
using WinVault.Model;
using WinVault.View;
using WinVault.ViewModel;
using GalaSoft.MvvmLight.Ioc;
using WinVault.Common;
using System;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Globalization;
using Windows.UI.Xaml.Data;

namespace WinVault {
   public sealed partial class HomePage {
      public HomePage() {
         InitializeComponent();
      }

      public HomeViewModel ViewModel {
         get {
            return (HomeViewModel)DataContext;
         }
      }

      private void pageTitle_SelectionChanged(object sender, RoutedEventArgs e) {

      }

      private void StopTaskProgressBar() {
         tasksProgressBar.IsIndeterminate = false;
         tasksProgressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      private void StopWorkflowsProgressBar() {
         workflowsProgressBar.IsIndeterminate = false;
         workflowsProgressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      protected override void OnNavigatedTo(NavigationEventArgs e) {
         base.OnNavigatedTo(e);
         ViewModel.MTProgressBar = StopTaskProgressBar;
         ViewModel.AWProgressBar = StopWorkflowsProgressBar;

         tasksProgressBar.IsIndeterminate = true;
         workflowsProgressBar.IsIndeterminate = true;

         showCounts();

         var myTasksList = ViewModel.getMyTasksList(StopTaskProgressBar);
         var activeWorkflowsList = ViewModel.getActiveWorkflowsList(StopWorkflowsProgressBar);

         this.gridViewMyTasks.ItemsSource = myTasksList;

         foreach (MyTask task in myTasksList) {

         }

         this.gridViewActiveWorkflows.ItemsSource = activeWorkflowsList;
      }

      protected override void OnNavigatedFrom(NavigationEventArgs e) {
         base.OnNavigatedFrom(e);
      }

      // Displays count of MyTasks and ActiveWorkflows
      private async void showCounts() {
         String ret = await ViewModel.getCounts();
         TaskCount.Text = ViewModel.TotalMyTasks.ToString();
         ActiveWorkflowCount.Text = ViewModel.TotalActiveWorkflows.ToString();
      }

      private void MyTaskSelectionChanged(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e) {
         //get the MyTask that the user selected
         MyTask myTask = (MyTask)e.AddedItems[0];
         //navigate to the MyTaskPage, pass in the MyTask ID
         SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.View.MyTasksDetailPage), myTask);
      }

      private void ActiveWorkflowSelectionChanged(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e) {
         //get the ActiveWorkflow that the user selected
         ActiveWorkflow activeWorkflow = (ActiveWorkflow)e.AddedItems[0];
         //navigate to the ActiveWorkflowsPage, pass in the ActiveWorkflow ID
         SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.View.ActiveWorkflowsDetailPage), activeWorkflow);
      }

   }
}