﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using WinVault.ViewModel;
using WinVault.Model;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Ioc;
using WinVault.Common;
using System;
using Windows.UI.Xaml.Controls;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Media;
using Windows.UI;
using System.Collections.Generic;
using Windows.UI.Popups;
using Windows.Storage;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238
namespace WinVault.View {
   public sealed partial class MyTasksDetailPage {
      public MyTasksDetailPage() {
         InitializeComponent();
      }

      public MyTaskDetailViewModel ViewModel {
         get {
            return (MyTaskDetailViewModel)DataContext;
         }
      }

      protected override void OnNavigatedTo(NavigationEventArgs e) {
         base.OnNavigatedTo(e);
         this.showMyTask((MyTask)e.Parameter);
      }

      private void StopAnimatingProgressBar() {
         progressBar.IsIndeterminate = false;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      private void StartAnimatingProgressBar() {
         progressBar.IsIndeterminate = true;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
      }

      private async void showMyTask(MyTask task) {
         if (ViewModel != null) {
            PageTitle.Text = task.TaskName;
            TaskDueDate.Text = task.TaskDueDate;
            Assignee.Text = task.TaskAssigneeName;

            // check for comments
            if (task.TaskComments.Length > 0) {
               Comments.Text = task.TaskComments;
            }
            else {
               CommentsLabel.Visibility = Visibility.Collapsed;
            }
            
            TaskAssignDate.Text = task.TaskAssignmentDate;
            string documentID = task.TaskDocumentID;

            StartAnimatingProgressBar();
            string documentFormat = await ViewModel.getDocumentFormat(documentID);
            if (documentFormat.Contains("video")) {
               this.renderVideo(documentID);
            }
            else {
               this.renderDocument(documentID);
            }

         }
         else {
            System.Diagnostics.Debug.WriteLine("ERROR: MyTaskDetailViewModel null");
         }
      }

      private async void renderVideo(string docID) {
         StorageFile videoFile = await ViewModel.getVideo(docID);

         if (videoFile != null) {
            //read the stream from the local storage file
            var stream = await videoFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
            //set the source to be the stream
            this.videoMediaElement.SetSource(stream, videoFile.ContentType);
            //show the media element
            this.videoMediaElement.Width = Window.Current.CoreWindow.Bounds.Width - 780;
            this.videoMediaElement.Visibility = Windows.UI.Xaml.Visibility.Visible;
            //hide the document view
            this.documentView.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
         }
         else {
            var msg = new MessageDialog("Could not open video.");
            msg.Title = "Error";
            msg.ShowAsync();
         }
         StopAnimatingProgressBar();
      }
      
      private async void renderDocument(string docID) {
         //tell the viewmodel to get the document that has the ID in docID
         ObservableCollection<Image> docPDF = await ViewModel.getDocument(docID);
         //if docPDF is not null then we succesfully received the document from Veeva's API
         if (docPDF != null) {
            foreach (Image page in docPDF) {
               page.Width = Window.Current.CoreWindow.Bounds.Width - 780;
               this.documentView.Items.Add(page);
            }
         }
         else {
            var msg = new MessageDialog("Could not open document.");
            msg.Title = "Error";
            msg.ShowAsync();
         }
         StopAnimatingProgressBar();
      }
   }
}
