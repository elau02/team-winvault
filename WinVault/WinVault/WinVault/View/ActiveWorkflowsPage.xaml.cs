﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using WinVault.ViewModel;
using WinVault.Model;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Ioc;
using WinVault.Common;
using System;
using System.Diagnostics;

namespace WinVault
{
    public sealed partial class ActiveWorkflowsPage
    {
        public ActiveWorkflowsPage()
        {
            InitializeComponent();
        }

        public ActiveWorkflowsViewModel ViewModel
        {
            get { return (ActiveWorkflowsViewModel)DataContext; }
        }

        private void StopAnimatingProgressBar() {
           progressBar.IsIndeterminate = false;
           progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ViewModel.AWProgressBar = StopAnimatingProgressBar;

            //tell the viewmodel to get the document list
            progressBar.IsIndeterminate = true;
            progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;

            var list = ViewModel.getActiveWorkflowsList(StopAnimatingProgressBar);
            //update the item source to be the return from the viewmodel
            this.activeWorkflowsGridView.ItemsSource = list;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private void ActiveWorkflowSelectionChanged(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e)
        {
            //get the ActiveWorkflow that the user selected
            ActiveWorkflow activeWorkflow = (ActiveWorkflow)e.AddedItems[0];
            //navigate to the ActiveWorkflowsPage, pass in the ActiveWorkflow ID
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.View.ActiveWorkflowsDetailPage), activeWorkflow);
        }
    }
}