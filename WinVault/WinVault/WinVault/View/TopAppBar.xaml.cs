﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinVault.Common;

namespace WinVault.View {
   public sealed partial class TopAppBar : UserControl {
      public TopAppBar() {
         this.InitializeComponent();
      }

      private void AppBarButton_Home(object sender, RoutedEventArgs e) {
         SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(HomePage));
      }

      private void AppBarButton_Library(object sender, RoutedEventArgs e) {
         SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(LibraryPage));
      }
   }
}
