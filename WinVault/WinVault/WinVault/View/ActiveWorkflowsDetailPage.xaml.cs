﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinVault.ViewModel;
using System.Collections.Generic;
using Windows.UI.Popups;
using System.Collections.ObjectModel;
using WinVault.Model;
using Windows.Storage;

namespace WinVault.View {
   public sealed partial class ActiveWorkflowsDetailPage {
      public ActiveWorkflowsDetailPage() {
         InitializeComponent();
      }

      public ActiveWorkflowDetailViewModel ViewModel {
         get {
            return (ActiveWorkflowDetailViewModel)DataContext;
         }
      }

      private void StopAnimatingProgressBar() {
         progressBar.IsIndeterminate = false;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      private void StartAnimatingProgressBar() {
         progressBar.IsIndeterminate = true;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
      }

      protected override void OnNavigatedTo(NavigationEventArgs e) {
         base.OnNavigatedTo(e);
         this.showActiveWorkflow((ActiveWorkflow)e.Parameter);
      }

      private async void showActiveWorkflow(ActiveWorkflow activeWorkflow) {
         if (ViewModel != null) {
            // Show information about this workflow
            PageTitle.Text = activeWorkflow.WorkflowName;
            WorkflowOwner.Text = activeWorkflow.WorkflowInitiator;
            WorkflowDueDate.Text = activeWorkflow.WorkflowDueDate;
            string documentID = activeWorkflow.WorkflowDocID;

            StartAnimatingProgressBar();
            string documentFormat = await ViewModel.getDocumentFormat(documentID);
            if (documentFormat.Contains("video")) {
               this.renderVideo(documentID);
            }
            else {
               this.renderDocument(documentID);
            }

            // Show tasks associated with this workflow in a grid
            var taskList = await ViewModel.getActiveWorkflowTasks(activeWorkflow.WorkflowID);
            if (taskList != null) {
               this.taskListGridView.ItemsSource = taskList;
            }
         }
         else {
            System.Diagnostics.Debug.WriteLine("ERROR: MyTaskDetailViewModel null");
         }

      }

      private async void renderVideo(string docID) {
         StorageFile videoFile = await ViewModel.getVideo(docID);

         if (videoFile != null) {
            //read the stream from the local storage file
            var stream = await videoFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
            //set the source to be the stream
            this.videoMediaElement.SetSource(stream, videoFile.ContentType);
            //show the media element
            this.videoMediaElement.Width = Window.Current.CoreWindow.Bounds.Width - 780;
            this.videoMediaElement.Visibility = Windows.UI.Xaml.Visibility.Visible;
            //hide the document view
            this.documentView.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
         }
         else {
            var msg = new MessageDialog("Could not open video.");
            msg.Title = "Error";
            msg.ShowAsync();
         }
         StopAnimatingProgressBar();
      }

      private async void renderDocument(string docID) {
         //tell the viewmodel to get the document that has the ID in docID
         ObservableCollection<Image> docPDF = await ViewModel.getDocument(docID);
         //if docPDF is not null then we succesfully received the document from Veeva's API
         if (docPDF != null) {
            foreach (Image page in docPDF) {
               page.Width = Window.Current.CoreWindow.Bounds.Width - 780;
               this.documentView.Items.Add(page);
            }
         }
         else {
            var msg = new MessageDialog("Could not open document.");
            msg.Title = "Error";
            msg.ShowAsync();
         }
         StopAnimatingProgressBar();
      }
   }
}
