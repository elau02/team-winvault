﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using WinVault.ViewModel;
using WinVault.Model;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Ioc;
using WinVault.Common;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls;

namespace WinVault {
   public sealed partial class LibraryPage {
      private GridView currentGridView;
      private IncrementalLoadingCollection<DocumentSource, Document> documentList;
      private IncrementalLoadingCollection<FilterMyDocumentSource, Document> myDocumentList;
      private IncrementalLoadingCollection<FilterFavoriteDocumentSource, Document> favDocumentList;
      private IncrementalLoadingCollection<FilterRecentDocumentSource, Document> recentDocumentList;

      public LibraryPage() {
         InitializeComponent();
         currentGridView = currentGridView == null ? libraryGridView : currentGridView;
      }

      public LibraryViewModel ViewModel {
         get {
            return (LibraryViewModel)DataContext;
         }
      }

      private void StartAnimatingProgressBar() {
         progressBar.IsIndeterminate = true;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
      }

      private void StopAnimatingProgressBar() {
         progressBar.IsIndeterminate = false;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      protected override void OnNavigatedTo(NavigationEventArgs e) {
         base.OnNavigatedTo(e);
         StartAnimatingProgressBar();
         ViewModel.ProgressBarAction += StopAnimatingProgressBar;

         string[] retArray = (string[])e.Parameter; //get the array that was returned
         //if the array is null or of empty size, navigation to the default library page was selected
         if (retArray == null || retArray.Length == 0) {
            //document case
            //tell the viewmodel to get the document list
            documentList = ViewModel.getDocumentList();
            //update the item source to be the return from the viewmodel
            this.currentGridView.ItemsSource = documentList;
         }
         //array is present, navigating to either a binder or section
         else {
            this.getBinderSectionList(retArray[0], retArray[1]);
            this.PageTitle.Text = ViewModel.changeTitle(retArray[0]);
         }
      }

      //Handles telling the viewmodel to get the list of documents that are inside a binder or section
      private async void getBinderSectionList(string docId, string docType) {
         var docList = await ViewModel.getBinderOrSectionList(docId, docType);
         //update the item source to be the return from the viewmodel
         if (docList != null) {
            this.currentGridView.ItemsSource = docList;
         }
         StopAnimatingProgressBar();
      }

      protected override void OnNavigatedFrom(NavigationEventArgs e) {
         base.OnNavigatedFrom(e);
      }

      private void DocumentSelectionChanged(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e) {
         //selection changed gets called twice, first during the OnNavigatedTo setting of the ItemsSource
         //second time during the actual selection(> -1 case)
         if (this.currentGridView.SelectedIndex > -1) {
            ViewModel.handleSelectionChanged(e);
         }
      }

      private void SearchButton_Click(object sender, RoutedEventArgs e) {
         SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(SearchPage));
      }
      
      private void AllDocumentsButton_Click(object sender, RoutedEventArgs e) {
         this.currentGridView.ItemsSource = documentList;
         this.MyDocumentsButton.IsChecked = false;
         this.RecentDocumentsButton.IsChecked = false;
         this.FavoritesButton.IsChecked = false;
      }

      private void MyDocumentsButton_Click(object sender, RoutedEventArgs e) {
         //tell the viewmodel to get the document list
         if (myDocumentList == null) {
            StartAnimatingProgressBar();
            myDocumentList = ViewModel.getMyDocumentList();
         }
         //update the item source to be the return from the viewmodel
         this.currentGridView.ItemsSource = myDocumentList;
         //this.AllDocumentsButton.IsChecked = false;
         this.RecentDocumentsButton.IsChecked = false;
         this.FavoritesButton.IsChecked = false;
      }

      private void RecentDocumentsButton_Click(object sender, RoutedEventArgs e) {
         //tell the viewmodel to get the document list
         if (recentDocumentList == null) {
            StartAnimatingProgressBar();
            recentDocumentList = ViewModel.getRecentDocumentList();
         }
         //update the item source to be the return from the viewmodel
         this.currentGridView.ItemsSource = recentDocumentList;
         this.MyDocumentsButton.IsChecked = false;
         //this.AllDocumentsButton.IsChecked = false;
         this.FavoritesButton.IsChecked = false;
      }

      private void FavoritesButton_Click(object sender, RoutedEventArgs e) {
         //tell the viewmodel to get the document list
         if (favDocumentList == null) {
            StartAnimatingProgressBar();
            favDocumentList = ViewModel.getFavoriteDocumentList();
         }
         //update the item source to be the return from the viewmodel
         this.currentGridView.ItemsSource = favDocumentList;
         this.MyDocumentsButton.IsChecked = false;
         this.RecentDocumentsButton.IsChecked = false;
      }

      private void OnSelectionChanged(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e) {
         if (this.currentGridView != null) {
            if (popupSort != null) {
               popupSort.IsOpen = false;
               this.currentGridView.Margin = new Thickness(0, 0, 0, 0);
               this.SortButton.IsChecked = false;
            }

            Windows.UI.Xaml.Controls.ListViewItem comboBoxItem = (Windows.UI.Xaml.Controls.ListViewItem)e.AddedItems[0];
            string selection = comboBoxItem.Content.ToString();
            System.Diagnostics.Debug.WriteLine(selection);
            //Set the selection parameter in the viewmodel
            ViewModel.setSortParameter(selection);
            //re-get the doc list
            documentList = ViewModel.getDocumentList();
            string context = ViewModel.Context;
            //simulate as if a button was pressed based off context(type of document viewing, EX: favorites, my recents)
            switch (context) {
               case "Favorites":
                  this.FavoritesButton_Click(null, null);
                  break;
               case "Recent Documents":
                  this.RecentDocumentsButton_Click(null, null);
                  break;
               case "My Documents":
                  this.MyDocumentsButton_Click(null, null);
                  break;
               default:
                  this.AllDocumentsButton_Click(null, null);
                  break;

            }
         }
      }

      private void SortButton_Checked(object sender, RoutedEventArgs e) {
         popupSort.IsOpen = true;
         currentGridView.Margin = new Thickness(300, 0, 0, 0);
      }

      private void SortButton_Unchecked(object sender, RoutedEventArgs e) {
         if (popupSort != null) {
            popupSort.IsOpen = false;
            currentGridView.Margin = new Thickness(0, 0, 0, 0);
         }
      }

      private void View2Button_Checked(object sender, RoutedEventArgs e) {
         View3Button.IsChecked = false;
         if (libraryImageOverlayLandingGrid.ItemsSource == null) {
            libraryImageOverlayLandingGrid.ItemsSource = documentList;
         }
         currentGridView.Visibility = Visibility.Collapsed;
         libraryImageOverlayLandingGrid.Visibility = Visibility.Visible;
         currentGridView = libraryImageOverlayLandingGrid;
      }

      private void ViewButton_Unchecked(object sender, RoutedEventArgs e) {
         currentGridView.Visibility = Visibility.Collapsed;
         libraryGridView.Visibility = Visibility.Visible;
         currentGridView = libraryGridView;
      }

      private void View3Button_Checked(object sender, RoutedEventArgs e) {
         View2Button.IsChecked = false;
         if (libraryImageTextListFileGrid.ItemsSource == null) {
            libraryImageTextListFileGrid.ItemsSource = documentList;
         }
         currentGridView.Visibility = Visibility.Collapsed;
         libraryImageTextListFileGrid.Visibility = Visibility.Visible;
         currentGridView = libraryImageTextListFileGrid;
      }
   }
}