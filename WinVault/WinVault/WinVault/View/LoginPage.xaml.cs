﻿using System;
using WinVault.ViewModel;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace WinVault {
   public sealed partial class LoginPage {
      /// <summary>
      /// Gets the view's ViewModel.
      /// </summary>
      public LoginViewModel ViewModel {
         get {
            return (LoginViewModel)DataContext;
         }
      }

      public LoginPage() {
         InitializeComponent();
         loginProgressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      private void HideProgressBar() {
         loginProgressBar.IsIndeterminate = false;
         loginProgressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      private void LoginButton_Click(object sender, RoutedEventArgs e) {
         loginProgressBar.IsIndeterminate = true;
         loginProgressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
         ViewModel.progressBarDelegate = HideProgressBar;
         ViewModel.HandleLogin();
      }
   }
}