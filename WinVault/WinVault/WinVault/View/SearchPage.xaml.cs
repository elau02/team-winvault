﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using WinVault.ViewModel;
using WinVault.Model;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Ioc;
using WinVault.Common;
using System;
using System.Windows.Input;

namespace WinVault {

   public sealed partial class SearchPage {

      public SearchPage() {
         InitializeComponent();
      }

      public SearchViewModel ViewModel {
         get {
            return (SearchViewModel)DataContext;
         }
      }

      protected override void OnNavigatedTo(NavigationEventArgs e) {
         base.OnNavigatedTo(e);
         StopAnimatingProgressBar();
      }

      private void StartAnimatingProgressBar() {
         progressBar.IsIndeterminate = true;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
      }

      private void StopAnimatingProgressBar() {
         progressBar.IsIndeterminate = false;
         progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
      }

      private void SearchButton_Click(object sender, RoutedEventArgs e) {
         getSearchResults();
      }

      private void getSearchResults() {
         if (!SearchBox.Text.Equals("")) {
            StartAnimatingProgressBar();
            var list = SearchViewModel.getSearchResults(SearchBox.Text, StopAnimatingProgressBar);
            this.ResultsGridView.ItemsSource = list;
         }
      }

      public void DocumentSelectionChanged(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e) {
         //get the document that the user selected
         Document doc = (Document)e.AddedItems[0];
         string[] strArray = new string[2]; //index 0: document id, index 1: document type
         strArray[0] = doc.ID;

         //Binder case: navigate to library page with binder as the type
         if (doc.IsBinder) {
            //tell the viewmodel to get the binder list
            strArray[1] = "binder";
            //_pageTitle = doc.DocumentName; //update page title to be name of binder or section
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.LibraryPage), strArray);
         }
         //Section case: navigate to library page with section as the type
         else if (doc.BinderElementMetadata != null && doc.BinderElementMetadata.IsSection) {
            //tell viewmodel to get the section contents list
            strArray[1] = "section";
            //_pageTitle = doc.DocumentName; //update page title to be name of binder or section
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.LibraryPage), strArray);
         }
         //Video case: navigate to the video page
         else if (doc.Format.Contains("video")) {
            //tell viewmodel we have a video to load
            strArray[1] = "video";
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.View.DocumentPage), strArray);
         }
         //Renderable document case: navigate to the selected document page
         else {
            //tell viewmodel we have a document to load
            strArray[1] = "document";
            //Note: WinVault.View.DocumentPage is needed due to an inconsistency of namespaces in our project
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.View.DocumentPage), strArray);
         }
      }

      private void SearchBox_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e) {
         if (e.Key == Windows.System.VirtualKey.Enter) {
            getSearchResults();
         }
      }
   }
}
