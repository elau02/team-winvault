﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model {
   public class BinderElement {
      public string InnerId { get; set; } //binder only, for sections this Id is unique to each binder
      public string ParentId { get; set; }//binder only
      public string Order { get; set; }//binder only
      public bool IsSection { get; set; } //section only
      public string SectionNumber { get; set; } //section only, optional field
      public string ParentBinderId { get; set; } //the id of the binder that this binder element is inside of

      public BinderElement(string innerId, string parentId, string order, string type, string sectionNumber, string parentBinderId) {
         this.InnerId = innerId;
         this.ParentId = parentId;
         this.Order = order;
         this.IsSection = type.Equals("section");
         this.SectionNumber = sectionNumber;
         this.ParentBinderId = parentBinderId;
      }
   }
}
