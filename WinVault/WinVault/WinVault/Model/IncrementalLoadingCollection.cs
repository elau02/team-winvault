﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Controls;
using WinVault.View;

//code taken from http://marcominerva.wordpress.com/2013/05/22/implementing-the-isupportincrementalloading-interface-in-a-window-store-app/comment-page-1/
namespace WinVault.Model {
   //IncrementalLoadingCollection class that is abstracted out using generics
   //This class sets up the automatic paging element of views
   public class IncrementalLoadingCollection<T, I> : ObservableCollection<I>,
        ISupportIncrementalLoading
        where T : IIncrementalSource<I>, new() {
      private T source;
      private int itemsPerPage;
      private bool hasMoreItems;
      private int currentPage;
      private int limit;
      private Action handler;

      //We can change the number to be loaded in each page through the constructor
      public IncrementalLoadingCollection(Action handler, int itemsPerPage = 5, int limit = int.MaxValue) {
         this.source = new T();
         this.itemsPerPage = itemsPerPage;
         this.hasMoreItems = true;
         this.limit = limit;
         this.handler = handler;
      }

      public bool HasMoreItems {
         get { return hasMoreItems; }
      }

      public void Stop() {
         hasMoreItems = false;
      }

      //All this code was taken from the website linked above
      public IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync(uint count) {
         var dispatcher = Window.Current.Dispatcher;

         return Task.Run<LoadMoreItemsResult>(
             async () => {
                uint resultCount = 0;
                //page for more items
                var result = await source.GetPagedItems(currentPage++, itemsPerPage);

                if (hasReachedLimit() || result == null || result.Count() == 0) {
                   hasMoreItems = false;
                   await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                     CoreDispatcherPriority.Normal, () => {
                        handler();
                     });
                   
                }
                else {
                   resultCount = (uint)result.Count();

                   await dispatcher.RunAsync(
                       CoreDispatcherPriority.Normal,
                       () => {
                          foreach (I item in result)
                             this.Add(item);
                       });
                }

                return new LoadMoreItemsResult() { Count = resultCount };

             }).AsAsyncOperation<LoadMoreItemsResult>();
      }

      private bool hasReachedLimit() {
         return (currentPage - 1) * itemsPerPage >= limit;
      }
   }
}
