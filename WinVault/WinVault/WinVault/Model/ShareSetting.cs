﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model
{
   public class ShareSetting {

      public string Name { get; set; }
      public List<string> Users { get; set; }
      public List<string> Groups { get; set; }

      public ShareSetting(string name, List<string> users, List<string> groups) {
         this.Name = name;
         this.Users = users;
         this.Groups = groups;
      }
   }
}
