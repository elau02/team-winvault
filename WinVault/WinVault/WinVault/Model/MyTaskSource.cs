﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Code taken from http://marcominerva.wordpress.com/2013/05/22/implementing-the-isupportincrementalloading-interface-in-a-window-store-app/comment-page-1/
namespace WinVault.Model
{
    //MyTaskSource class represents the collection that the view will use to update its view
    //In this case that is a List<MyTask>
    public class MyTaskSource : IIncrementalSource<MyTask>
    {
       private ObservableCollection<MyTask> MyTasksList = new ObservableCollection<MyTask>();

        public MyTaskSource()
        {
        }

        //When the user scrolls to the end of the page, this method gets called to get more paged items
        //Each time the GetPagedItems method will do an API call for 5 tasks
        public async Task<IEnumerable<MyTask>> GetPagedItems(int pageIndex, int pageSize)
        {
            return await Task.Run<IEnumerable<MyTask>>(async () =>
            {
                //Wait for the documents call to finish
                //pageSize = 5, pageIndex depends on how far the user has scrolled
                MyTasksList = await VeevaAPI.GetMyTasks(pageIndex * pageSize, pageSize);
                //for each task in the list, update the view
                var result = (from task in MyTasksList
                              select task).Skip(pageIndex * pageSize).Take(pageSize);

                return result;
            });
        }
    }
}
