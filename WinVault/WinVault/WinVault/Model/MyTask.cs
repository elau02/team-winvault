﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using System.Windows;
using Windows.UI.Xaml;

namespace WinVault.Model {
   /*
    * MyTask class used to store metadata about a task to display on the My Tasks Page
    */
   public class MyTask : INotifyPropertyChanged {

      public event PropertyChangedEventHandler PropertyChanged;
      private string p1;
      private string p2;
      private string p3;

      private string _taskID { get; set; }
      public string TaskID {
         get { return _taskID; }
         set {
            if (_taskID == value) { return; }
            _taskID = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskID"));
         }
      }

      private string _taskName { get; set; }
      public string TaskName {
         get { return _taskName; }
         set {
            if (_taskName == value) { return; }
            _taskName = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskName"));
         }
      }

      private string _taskAssigneeID { get; set; }
      public string TaskAssigneeID {
         get { return _taskAssigneeID; }
         set {
            if (_taskAssigneeID == value) { return; }
            _taskAssigneeID = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskAssigneeID"));
         }
      }

      private string _taskAssigneeName { get; set; }
      public string TaskAssigneeName {
         get { return _taskAssigneeName; }
         set {
            if (_taskAssigneeName == value) { return; }
            _taskAssigneeName = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskAssigneeName"));
         }
      }

      private string _taskStatus { get; set; }
      public string TaskStatus {
         get { return _taskStatus; }
         set {
            if (_taskStatus == value) { return; }
            _taskStatus = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskStatus"));
         }
      }

      private string _taskAssignmentDate { get; set; }
      public string TaskAssignmentDate {
         get { return _taskAssignmentDate; }
         set {
            if (_taskAssignmentDate == value) { return; }
            _taskAssignmentDate = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskAssignmentDate"));
         }
      }

      private string _taskCancellationDate { get; set; }
      public string TaskCancellationDate {
         get { return _taskCancellationDate; }
         set {
            if (_taskCancellationDate == value) { return; }
            _taskCancellationDate = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskCancellationDate"));
         }
      }

      private string _taskCompletionDate { get; set; }
      public string TaskCompletionDate {
         get { return _taskCompletionDate; }
         set {
            if (_taskCompletionDate == value) { return; }
            _taskCompletionDate = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskCompletionDate"));
         }
      }

      private string _taskCreationDate { get; set; }
      public string TaskCreationDate {
         get { return _taskCreationDate; }
         set {
            if (_taskCreationDate == value) { return; }
            _taskCreationDate = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskCreationDate"));
         }
      }

      private string _taskDueDate { get; set; }
      public string TaskDueDate {
         get { return _taskDueDate; }
         set {
            if (_taskDueDate == value) { return; }
            _taskDueDate = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskDueDate"));
         }
      }

      private string _taskDuration { get; set; }
      public string TaskDuration {
         get { return _taskDuration; }
         set {
            if (_taskDuration == value) { return; }
            _taskDuration = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskDuration"));
         }
      }

      private string _taskQueueGroup { get; set; }
      public string TaskQueueGroup {
         get { return _taskQueueGroup; }
         set {
            if (_taskQueueGroup == value) { return; }
            _taskQueueGroup = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskQueueGroup"));
         }
      }

      private string _taskVerdict { get; set; }
      public string TaskVerdict {
         get { return _taskVerdict; }
         set {
            if (_taskVerdict == value) { return; }
            _taskVerdict = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskVerdict"));
         }
      }

      private string _taskComments { get; set; }
      public string TaskComments {
         get { return _taskComments; }
         set {
            if (_taskComments == value) { return; }
            _taskComments = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskComments"));
         }
      }

      private string _taskDocumentID { get; set; }
      public string TaskDocumentID
      {
          get { return _taskDocumentID; }
          set
          {
              if (_taskDocumentID == value) { return; }
              _taskDocumentID = value;
              var handler = PropertyChanged;
              if (handler != null)
                  handler(this, new PropertyChangedEventArgs("TaskDocumentID"));
          }
      }

      private string _taskDocumentName { get; set; }
      public string TaskDocumentName {
         get { return _taskDocumentName; }
         set {
            if (_taskDocumentName == value) { return; }
            _taskDocumentName = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("TaskDocumentName"));
         }
      }

      private Boolean _overdue { get; set; }
      public Boolean IsOverdue {
         get { return _overdue; }
         set {
            if (_overdue == value) { return; }
            _overdue = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("IsOverdue"));
         }
      }

      private Boolean _timeLeft { get; set; }
      public Boolean IsTimeLeft {
         get { return _timeLeft; }
         set {
            if (_overdue == value) { return; }
            _timeLeft = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("IsTimeLeft"));
         }
      }

      public MyTask(string ID,
          string name,
          string assigneeID,
          string assigneeName,
          string status,
          string assignmentDate,
          string cancellationDate,
          string completionDate,
          string creationDate,
          string dueDate,
          string duration,
          string queueGroup,
          string verdict,
          string comments,
          string documentID,
          string documentName) {
         this.TaskID = ID;
         this.TaskName = name;
         this.TaskAssigneeID = assigneeID;
         this.TaskAssigneeName = assigneeName;
         this.TaskStatus = status;
         this.TaskAssignmentDate = assignmentDate;
         this.TaskCancellationDate = cancellationDate;
         this.TaskCompletionDate = completionDate;
         this.TaskCreationDate = creationDate;
         this.TaskDueDate = dueDate;
         this.TaskDuration = duration;
         this.TaskQueueGroup = queueGroup;
         this.TaskVerdict = verdict;
         this.TaskComments = comments;
         this.TaskDocumentID = documentID;
         this.TaskDocumentName = "\"" + documentName + "\"";

         DateTime dueDateTime = DateTime.Parse(dueDate);
         // if due date is earlier than today
         if (dueDateTime.CompareTo(DateTime.Now) < 0) {
            this.IsOverdue = true;
            this.IsTimeLeft = false;
         }
         else {
            this.IsOverdue = false;
            this.IsTimeLeft = true;
         }
      }

      // Home Page only needs task id, task name, task assignee, task status, task assignment date, task due date, task document ID
      public MyTask(string ID,
          string name,
          string assigneeName,
          string status,
          string assignmentDate,
          string dueDate,
          string documentID,
          string taskComments,
          string documentName) {
         this.TaskID = ID;
         this.TaskName = name;
         this.TaskAssigneeName = assigneeName;
         this.TaskStatus = status;
         this.TaskDueDate = dueDate;
         this.TaskDocumentID = documentID;
         this.TaskComments = taskComments;
         this.TaskDocumentName = "\"" + documentName + "\"";

         // Format assignment date similar to web interface
         DateTime assignDateTime = DateTime.Parse(assignmentDate);
         var datefmt = new Windows.Globalization.DateTimeFormatting.DateTimeFormatter("{month.full} {day.integer}");
         this.TaskAssignmentDate = datefmt.Format(assignDateTime);

         // Format due date
         DateTime dueDateTime = DateTime.Parse(dueDate);
         datefmt = new Windows.Globalization.DateTimeFormatting.DateTimeFormatter("{dayofweek.full} {month.full} {day.integer}");
         this.TaskDueDate = datefmt.Format(dueDateTime);

         // Determine if this task is overdue
         // if due date is earlier than today
         if (dueDateTime.CompareTo(DateTime.Now) < 0) {
            this.IsOverdue = true;
            this.IsTimeLeft = false;
         }
         else {
            this.IsOverdue = false;
            this.IsTimeLeft = true;
         }
      }

      public MyTask(string name, string assigneeName, string dueDate, string status) {
         this.TaskName = name;
         this.TaskAssigneeName = assigneeName;
         this.TaskStatus = status;

         // Format due date
         DateTime dueDateTime = DateTime.Parse(dueDate);
         var datefmt = new Windows.Globalization.DateTimeFormatting.DateTimeFormatter("{dayofweek.full} {month.full} {day.integer}");
         this.TaskDueDate = datefmt.Format(dueDateTime);

         // Determine if this task is overdue
         // if due date is earlier than today
         if (dueDateTime.CompareTo(DateTime.Now) < 0) {
            this.IsOverdue = true;
            this.IsTimeLeft = false;
         }
         else {
            this.IsOverdue = false;
            this.IsTimeLeft = true;
         }
      }
   }
}
