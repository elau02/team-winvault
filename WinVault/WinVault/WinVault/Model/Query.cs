﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model {
   public static class Query {

      //query for single task
      public static string Task(string taskID) {
         return "query?q=Select+" +
               "task_id__v," +
               "task_name__v," +
               "task_assignee__v," +
               "task_assignee_name__v," +
               "task_status__v," +
               "task_assignmentDate__v," +
               "task_cancelationDate__v," +
               "task_completionDate__v," +
               "task_creationDate__v," +
               "task_dueDate__v," +
               "task_duration__v," +
               "task_queueGroup__v," +
               "task_verdict__v," +
               "task_comment__v," +
               "workflow_document_id__v" +
               "+From+workflows+" +
               "WHERE+task_id__v+=+" +
               taskID;
      }

      //query for tasks associated with current logged in user
      public static string MyTasks {
         // Home Page only needs task id, task name, task assignee, task status, task assignment date, task due date, task document ID
         get {
            return "query?q=Select+" +
               "task_id__v," +
               "task_name__v," +
               "task_assignee_name__v," +
               "task_status__v," +
               "task_assignmentDate__v," +
               "task_dueDate__v," +
               "workflow_document_id__v," +
               "task_comment__v" +
               "+From+workflows+" +
               "WHERE+task_status__v='Assigned'+AND+task_assignee__v+=+" +
               User.Instance.UserId.ToString();
         }
      }

      //non intensive version of MyTasks. Useful for getting count
      public static string MySimpleTasks {
         get {
            return "query?q=Select+" +
                     "task_id__v+" +
                     "FROM+workflows+" +
                     "WHERE+task_status__v='Assigned'+AND+task_assignee__v+=+" + // Don't count tasks cancelled or completed.
                     User.Instance.UserId.ToString();
         }
      }

      //query for active workflow by document ID
      public static string ActiveWorkflowByDocumentID(string documentID) {
         return "query?q=Select+" +
                  "workflow_id__v," +
                  "workflow_document_id__v," +
                  "workflow_name__v," +
                  "workflow_initiator_name__v," +
                  "workflow_status__v," + // should only get status = Active
                  "workflow_dueDate__v+" +
                  "FROM+workflows+" +
                  "WHERE+workflow_status__v='Active'+" +
                  "AND+workflow_document_id__v+=+" +
                  documentID;
      }

      //query for active workflow detail for a single active workflow
      public static string ActiveWorkflowTasks(string activeWorkflowID) {
         return "query?q=Select+" +
                  "workflow_id__v," +
                  "task_name__v," +
                  "task_assignee_name__v," +
                  "task_dueDate__v,+" +
                  "task_status__v+" +
                  "From+workflows+" +
                  "WHERE+workflow_id__v+=+" +
                  activeWorkflowID;
      }

      //returns active workflow data for current user
      public static string MyActiveWorkflows {
         // home page only needs id, document id, name, status, dueDate.
         get {
            return "query?q=Select+" +
                     "workflow_id__v," +
                     "workflow_document_id__v," +
                     "workflow_name__v," +
                     "workflow_initiator_name__v," +
                     "workflow_status__v," + // should only get status = Active
                     "workflow_dueDate__v+" +
                     "FROM+workflows+" +
                     "WHERE+workflow_status__v='Active'+" +
                     "AND+workflow_initiator__v=" +
                     User.Instance.UserId.ToString();
         }
      }

      public static string MySimpleActiveWorkflows {
         get {
            return "query?q=Select+" +
                     "workflow_id__v+" +
                     "FROM+workflows+" +
                     "WHERE+workflow_status__v='Active'+" +
                     "AND+workflow_initiator__v=" +
                     User.Instance.UserId.ToString();
         }
      }
   }

}
