﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model
{
    public class DocumentPropertyMetadata
    {
        public string Name { get; set; }
        public string Section { get; set; }
        public string Label { get; set; }
        public bool IsRequired { get; set; }
        public bool CanRepeat { get; set; } // means it's something you can iterate through
        public bool IsEditable { get; set; }
        public bool IsHidden { get; set; }
        public bool IsQueryable { get; set; }

        public DocumentPropertyMetadata (string name, string section, string lbl, bool req, bool repeat, bool edit, bool hidden, bool query)
        {
            this.Name = name;
            this.Section = section;
            this.Label = lbl;
            this.IsRequired = req;
            this.CanRepeat = repeat;
            this.IsEditable = edit;
            this.IsHidden = hidden;
            this.IsQueryable = query;
        }
    }
}
