﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Code taken from http://marcominerva.wordpress.com/2013/05/22/implementing-the-isupportincrementalloading-interface-in-a-window-store-app/comment-page-1/
namespace WinVault.Model {
   //DocumentSource class represents the collection that the view will use to update its view
   //In this case that is a List<Document>
   public class DocumentSource : IIncrementalSource<Document> {
      private ObservableCollection<Document> DocumentList = new ObservableCollection<Document>();

      public DocumentSource() {
      }

      //When the user scrolls to the end of the page, this method gets called to get more paged items
      //Each time the GetPagedItems method will do an API call for 5 documents
      public virtual async Task<IEnumerable<Document>> GetPagedItems(int pageIndex, int pageSize) {
         return await Task.Run<IEnumerable<Document>>(async () => {
            //Wait for the documents call to finish
            //pageSize = 5, pageIndex depends on how far the user has scrolled
            ObservableCollection<Document> tempList = await VeevaAPI.getDocuments(pageIndex * pageSize, pageSize);
            foreach (Document tempDoc in tempList) {
               DocumentList.Add(tempDoc);
            }
            //for each document in the list, update the view
            return (from doc in DocumentList
                    select doc).Skip(pageIndex * pageSize).Take(pageSize);
         });
      }
   }
}
