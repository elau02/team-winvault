﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace WinVault.Model
{
    public class ActiveWorkflow : INotifyPropertyChanged 
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _workflowID { get; set; }
        public string WorkflowID
        {
            get { return _workflowID; }
            set
            {
                if (_workflowID == value) { return; }
                _workflowID = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowID"));
            }
        }
        
        private string _workflowDocID { get; set; }
        public string WorkflowDocID
        {
            get { return _workflowDocID; }
            set
            {
                if (_workflowDocID == value) { return; }
                _workflowDocID = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowDocID"));
            }
        }

        private string _workflowDocMinorVersionNum { get; set; }
        public string WorkflowDocMinorVersionNum
        {
            get { return _workflowDocMinorVersionNum; }
            set
            {
                if (_workflowDocMinorVersionNum == value) { return; }
                _workflowDocMinorVersionNum = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowDocMinorVersionNum"));
            }
        }

        private string _workflowDocMajorVersionNum { get; set; }
        public string WorkflowDocMajorVersionNum
        {
            get { return _workflowDocMajorVersionNum; }
            set
            {
                if (_workflowDocMajorVersionNum == value) { return; }
                _workflowDocMajorVersionNum = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowDocMinorVersionNum"));
            }
        }

        private string _workflowName { get; set; }
        public string WorkflowName
        {
            get { return _workflowName; }
            set
            {
                if (_workflowName == value) { return; }
                _workflowName = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowName"));
            }
        }

        private string _workflowInitiator { get; set; }
        public string WorkflowInitiator
        {
            get { return _workflowInitiator; }
            set
            {
                if (_workflowInitiator == value) { return; }
                _workflowInitiator = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowInitiator"));
            }
        }

        private string _workflowInitiatorName { get; set; }
        public string WorkflowInitiatorName
        {
            get { return _workflowInitiatorName; }
            set
            {
                if (_workflowInitiatorName == value) { return; }
                _workflowInitiator = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowInitiatorName"));
            }
        }

        private string _workflowStatus { get; set; }
        public string WorkflowStatus
        {
            get { return _workflowStatus; }
            set
            {
                if (_workflowStatus == value) { return; }
                _workflowStatus = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowStatus"));
            }
        }

        private string _workflowStartDate { get; set; }
        public string WorkflowStartDate
        {
            get { return _workflowStartDate; }
            set
            {
                if (_workflowStartDate == value) { return; }
                _workflowStartDate = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowStartDate"));
            }
        }

        private string _workflowCancellationDate { get; set; }
        public string WorkflowCancellationDate
        {
            get { return _workflowCancellationDate; }
            set
            {
                if (_workflowCancellationDate == value) { return; }
                _workflowStartDate = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowCancellationDate"));
            }
        }

        private string _workflowCompleteDate { get; set; }
        public string WorkflowCompleteDate
        {
            get { return _workflowCompleteDate; }
            set
            {
                if (_workflowCompleteDate == value) { return; }
                _workflowCompleteDate = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowCompleteDate"));
            }
        }

        private string _workflowDueDate { get; set; }
        public string WorkflowDueDate
        {
            get { return _workflowDueDate; }
            set
            {
                if (_workflowDueDate == value) { return; }
                _workflowDueDate = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowDueDate"));
            }
        }

        private string _workflowDuration { get; set; }
        public string WorkflowDuration
        {
            get { return _workflowDuration; }
            set
            {
                if (_workflowDuration == value) { return; }
                _workflowDuration = value;
                var handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs("WorkflowDuration"));
            }
        }

        private string _workflowDocumentName { get; set; }
        public string WorkflowDocumentName {
           get { return _workflowDocumentName; }
           set {
              if (_workflowDocumentName == value) { return; }
              _workflowDocumentName = value;
              var handler = PropertyChanged;
              if (handler != null)
                 handler(this, new PropertyChangedEventArgs("WorkflowDocumentName"));
           }
        }

        private Boolean _overdue { get; set; }
        public Boolean IsOverdue {
           get { return _overdue; }
           set {
              if (_overdue == value) { return; }
              _overdue = value;
              var handler = PropertyChanged;
              if (handler != null)
                 handler(this, new PropertyChangedEventArgs("IsOverdue"));
           }
        }

        private Boolean _timeLeft { get; set; }
        public Boolean IsTimeLeft {
           get { return _timeLeft; }
           set {
              if (_overdue == value) { return; }
              _timeLeft = value;
              var handler = PropertyChanged;
              if (handler != null)
                 handler(this, new PropertyChangedEventArgs("IsTimeLeft"));
           }
        }

        // home page only needs id, document id, name, status, dueDate.
        public ActiveWorkflow(string id,
                     string docId,
                     string name,
                     string initiatorName,
                     string status,
                     string dueDate,
                     string docName) {
           this.WorkflowID = id;
           this.WorkflowDocID = docId;
           this.WorkflowName = name;
           this.WorkflowInitiatorName = initiatorName;
           this.WorkflowStatus = status;
           this.WorkflowDocumentName = docName;

           // Format due date
           DateTime dueDateTime = DateTime.Parse(dueDate);
           var datefmt = new Windows.Globalization.DateTimeFormatting.DateTimeFormatter("{dayofweek.full} {month.full} {day.integer}");
           this.WorkflowDueDate = datefmt.Format(dueDateTime);

           // Determine if this task is overdue
           // if due date is earlier than today
           if (dueDateTime.CompareTo(DateTime.Now) < 0) {
              this.IsOverdue = true;
              this.IsTimeLeft = false;
           }
           else {
              this.IsOverdue = false;
              this.IsTimeLeft = true;
           }
        }
    }
}
