﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model
{
    public class ActiveWorkflowSource : IIncrementalSource<ActiveWorkflow>
    {
        private ObservableCollection<ActiveWorkflow> ActiveWorkflowsList = new ObservableCollection<ActiveWorkflow>();

        public async Task<IEnumerable<ActiveWorkflow>> GetPagedItems(int pageIndex, int pageSize)
        {
            return await Task.Run<IEnumerable<ActiveWorkflow>>(async () =>
            {
                ActiveWorkflowsList = await VeevaAPI.GetActiveWorkflows(pageIndex * pageSize, pageSize);
                var result = (from workflow in ActiveWorkflowsList
                              select workflow).Skip(pageIndex * pageSize).Take(pageSize);
                return result;
            });
        }
    }
}
