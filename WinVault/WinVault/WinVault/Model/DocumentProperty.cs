﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model
{
    public class DocumentProperty<T> : IDocumentProperty
    {
        public string Name { get; set; }
        public T Value { get; set; }
        public string Label { get; set; }

        public DocumentProperty(string name, T value, string lbl)
        {
            this.Name = name;
            this.Value = value;
            this.Label = lbl;
        }
    }
}
