﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model {
   public class Vault {
      public int Id { get; set; }
      public string Name { get; set; }
      public Uri Url { get; set; }
      public List<DocumentPropertyMetadata> PropertyMetadata { get; set; }

      public Vault() {
          PropertyMetadata = new List<DocumentPropertyMetadata>();
      }
   }
}
