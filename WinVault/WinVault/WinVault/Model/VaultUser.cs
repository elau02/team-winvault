﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model {
   /// <summary>
   /// Represents a vault user from the Vault API when retreiving a user
   /// </summary>
   public class VaultUser {
      public int Id { get; set; }
      public string Username { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public string FullName {
         get { return FirstName + " " + LastName; }
      }

      public VaultUser() { }
      public VaultUser(int id, string un, string fn, string ln) {
         Id = id;
         Username = un;
         FirstName = fn;
         LastName = ln;
      }
   }
}
