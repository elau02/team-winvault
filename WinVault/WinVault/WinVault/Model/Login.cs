﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model {
   public class Login : INotifyPropertyChanged {

      public event PropertyChangedEventHandler PropertyChanged;
      private string _userName;
      public string UserName {
         get { return _userName; }
         set {
            if (_userName == value) { return; }
            _userName = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("UserName"));
         }
      }

      private string _password;
      public string Password {
         get { return _password; }
         set {
            if (_password == value) { return; }
            _password = value;
            var handler = PropertyChanged;
            if (handler != null)
               handler(this, new PropertyChangedEventArgs("Password"));
         }
      }

      public Login() {
         UserName = "";
         Password = "";
      }

      public void Reset() {
         UserName = "";
         Password = "";
      }

      public Login(string username, string pw) {
         UserName = username;
         Password = pw;
      }  
   }
}
