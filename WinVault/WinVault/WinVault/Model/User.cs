﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model {
   public class User {
      public static User instance;
      public string SessionId { get; set; }
      public int UserId { get; set; }
      public int VaultId { get; set; }
      public List<Vault> Vaults { get; set; }

      private User() { }

      public static User Instance {
         get {
            if (instance == null)
               instance = new User();
            return instance;
         }
      }

      public void SetUser(dynamic user) {
         Vaults = user.vaultIds.ToObject<List<Vault>>();
         UserId = user.userId;
         SessionId = (string)user.sessionId;
      }
   }
}
