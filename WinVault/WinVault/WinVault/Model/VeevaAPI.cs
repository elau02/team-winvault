﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Net.Http.Headers;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using System.IO;
using System.Diagnostics;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using System.Collections.ObjectModel;
using Windows.Storage;
using Windows.Data.Pdf;
using Windows.Storage.Streams;
using Windows.Storage.AccessCache;
using Windows.ApplicationModel.Resources.Core;

namespace WinVault.Model {

   public class VeevaAPI {
      private static Uri _baseAddress;
      public static Uri BaseAddress {
         get { return _baseAddress; }
         set {
            _baseAddress = value;
            //client needs to be re-initalized when changing a property
            client = new HttpClient();
            client.BaseAddress = _baseAddress;
         }
      }
      private static HttpClient client;
      public static int LibrarySize;

      private static Dictionary<string, Document> documentDictionary = new Dictionary<string, Document>();
      private static Dictionary<string, VaultUser> userDictionary = new Dictionary<string, VaultUser>();
      private static List<String> documentTypes;
      private static Dictionary<String, String> documentProducts;

      public static string SearchTerm { get; set; }

      public static string version = "v7.0";
      public static string sortParameter = "name__v asc";

      public delegate void VoidDelegate();
      public delegate void DynamicDelegate(dynamic res);

      // we'll use three for now.
      // 0 for no internet
      // 1 for timed out
      // 2 for something else...?
      public static Action[] ErrorHandlingDelegates = new Action[3];

      public VeevaAPI() {
         client = new HttpClient();
         client.Timeout = new TimeSpan(0, 0, 10);
      }

      //used to get last connected vault (from online version)
      public async static Task Login(Login user, DynamicDelegate success, VoidDelegate badCredentials, VoidDelegate noInternet, VoidDelegate timeout) {
         var content = new FormUrlEncodedContent(new[] {
           new KeyValuePair<string, string>("username", user.UserName),
           new KeyValuePair<string, string>("password", user.Password)
         });

         try {
            var result = await client.PostAsync("https://login.veevavault.com/auth/api", content);

            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            if (((String)res.responseStatus).Equals("SUCCESS")) {
               if (BaseAddress == null)
                  BaseAddress = new Uri((String)res.defaultUrl + "/" + version + "/");

               success(res);
            }
            else {
               badCredentials();
            }
         }
         catch (HttpRequestException ex) {
            noInternet();
         }
         // there's an msdn error where httpclient is supposed to throw a webexception, not a taskcanceledexception on timeout
         catch (TaskCanceledException ex) {
            // connection timed out
            timeout();
         }
         catch (Exception ex) {
            // some other error happened
         }
      }

      //this call needs to be done to get userId
      public async static Task SetUserInformation(Login user, VoidDelegate noInternet, VoidDelegate timeout) {
         var content = new FormUrlEncodedContent(new[] {
           new KeyValuePair<string, string>("username", user.UserName),
           new KeyValuePair<string, string>("password", user.Password)
         });

         using (var client2 = new HttpClient()) {
            try {
               var userResult = client2.PostAsync(BaseAddress + "auth", content).Result;
               dynamic userRes = JObject.Parse(userResult.Content.ReadAsStringAsync().Result);
               User.Instance.SetUser(userRes);
               client.DefaultRequestHeaders.Add("Authorization", (string)userRes.sessionId);

               
               await SetDocumentTypes(); //set the doc types available for vault
               await SetDocumentProducts(); //set the doc products available for vault
            }
            catch (HttpRequestException ex) {
               noInternet();
            }
            catch (TaskCanceledException ex) {
               timeout();
            }
            catch (Exception ex) {
               // some other error happened
            }
         }
      }

      public static void Logout() {
         client.DefaultRequestHeaders.Clear();
      }

      //Call to receive only the library size, currently not used but might be useful in future
      public async static Task<int> getLibrarySize() {
         try {
            HttpResponseMessage result = await client.GetAsync("objects/documents/?" + "start=" + 0 + "&limit=" + 1);
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            VeevaAPI.LibrarySize = (int)res.size;
            return (int)res.size;
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return 0;
      }

      public async static Task<String> GetDocumentNameById(String Id) {
         try {
            //Sort parameter allows for the API to return the list in a sorted manner, currently sorted by name in descending order
            HttpResponseMessage result = await client.GetAsync("objects/documents/" + Id);
            //Wait for the GET request to finish, then continue to update the GUI
            //parse json results
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JObject documentObj = (JObject)res.GetValue("document");
            if (documentObj != null) {
               return documentObj.GetValue("name__v").ToString() == null ? "" : documentObj.GetValue("name__v").ToString();
            }
            return null;
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return null;
      }

      //returns one document with all its metadata, currently used in testing of metadata
      public async static Task<Document> GetDocumentById(string Id) {
         try {
            //Sort parameter allows for the API to return the list in a sorted manner, currently sorted by name in descending order
            HttpResponseMessage result = await client.GetAsync("objects/documents/" + Id);
            //Wait for the GET request to finish, then continue to update the GUI
            //parse json results
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JObject documentObj = (JObject)res.GetValue("document");
            return await VeevaAPI.obtainMetadata(documentObj);
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return null;
      }

      public async static Task<ObservableCollection<Document>> getBinderOrSectionList(string Id, string binderElementType) {
         bool isBinder = binderElementType.Equals("binder");
         ObservableCollection<Document> _documentList = new ObservableCollection<Document>();

         try {
            HttpResponseMessage result;
            //change API call depending on if its a section or a binder
            result = isBinder ? await client.GetAsync("objects/binders/" + Id) :
               await client.GetAsync("objects/binders/" + documentDictionary[Id].BinderElementMetadata.ParentBinderId + "/sections/" + Id);
            //parse results
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            //JObject for the document's metadata, don't need this if we are at a section
            JObject document = isBinder ? (JObject)res.document : null;

            JObject outerObject = isBinder ? (JObject)res.binder : (JObject)res.node;
            JArray nodesList = (JArray)outerObject.GetValue("nodes");

            //go through each node in the nodes array
            foreach (JObject nodeObj in nodesList) {
               JObject nodeProperties = (JObject)nodeObj.GetValue("properties");
               string innerId = nodeProperties.GetValue("id").ToString(); //id internal to binder
               string parentId = nodeProperties.GetValue("parent_id__v").ToString(); //Internal veeva parent id, can be a string or an int, not used in our app
               string order = nodeProperties.GetValue("order__v").ToString();
               string type = nodeProperties.GetValue("type__v").ToString(); //type of document, binder/section/doc
               string name = nodeProperties.GetValue("name__v").ToString(); //name of the document
               string id = ""; //sections dont have ids..., actual id of document outside of binder or section
               string sectionNumber = ""; //only sections have section numbers
               if (!type.Equals("section")) {
                  id = nodeProperties.GetValue("document_id__v").ToString();
               }
               else {
                  //sectionnumber is an optional field, usually null
                  sectionNumber = nodeProperties.GetValue("section_number__v").ToString() == null ? "" : nodeProperties.GetValue("section_number__v").ToString();
               }
               //ParentBinderId is the ID of the upper binder that this sub element is associated with
               string parentBinderId = isBinder ? document.GetValue("id").ToString() : documentDictionary[Id].BinderElementMetadata.ParentBinderId;

               BinderElement binElement = new BinderElement(innerId, parentId, order, type, sectionNumber, parentBinderId);

               //section inside of a binder or section
               if (type.Equals("section")) {
                  //manually create a "document" for the section
                  //cannot use obtainmetadata since sections dont have the same properties
                  Document section = new Document();
                  section.DocumentName = name;
                  section.ID = binElement.InnerId; //this ID is the unique ID for a section inside of a binder
                  section.BinderElementMetadata = binElement;
                  //set section image
                  BitmapImage image = new BitmapImage();
                  image.UriSource = new Uri("ms-appx:///Assets/section.png", UriKind.Absolute);
                  section.ThumbnailImage = image;

                  _documentList.Add(section);
                  if (!documentDictionary.ContainsKey(binElement.InnerId))
                     documentDictionary.Add(binElement.InnerId, section);
               }
               //all other documents
               else {
                  HttpResponseMessage docResult;
                  //get the properities that are not associated with the document being in a binder. AKA regular document metadata
                  if (type.Equals("binder")) {
                     docResult = await client.GetAsync("objects/binders/" + id);
                  }
                  else {
                     docResult = await client.GetAsync("objects/documents/" + id);
                  }
                  docResult.EnsureSuccessStatusCode();
                  //parse json results
                  dynamic docRes = JObject.Parse(docResult.Content.ReadAsStringAsync().Result);
                  JObject documentObj = (JObject)docRes.document;
                  Document binderDoc = await obtainMetadata(documentObj);
                  binderDoc.BinderElementMetadata = binElement;
                  _documentList.Add(binderDoc);
                  if (!documentDictionary.ContainsKey(binderDoc.ID))
                     documentDictionary.Add(binderDoc.ID, binderDoc);
               }

            }
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return _documentList;
      }

      //getDocuments method takes in a starting point and a limit to stop at
      //EX: getDocuments(0, 5) returns documents 0 to 4
      public async static Task<ObservableCollection<Document>> getDocuments(int start, int limit) {
         ObservableCollection<Document> _documentList = new ObservableCollection<Document>();

         try {
            //Sort parameter allows for the API to return the list in a sorted manner
            HttpResponseMessage result = await client.GetAsync("objects/documents/?" + "start=" + start + "&limit=" + limit + "&sort=" + VeevaAPI.sortParameter);
            //Wait for the GET request to finish, then continue to update the GUI
            //parse json results
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            JArray documentList = (JArray)res.documents;
            foreach (JObject obj in documentList) {
               JObject documentObj = (JObject)obj.GetValue("document");
               Document doc = await VeevaAPI.obtainMetadata(documentObj);
               _documentList.Add(doc);
               if (!documentDictionary.ContainsKey(doc.ID))
                  documentDictionary.Add(doc.ID, doc);
            }
         }
         catch (HttpRequestException ex) {
            // no internet
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[0];
                  action();
               });
         }
         catch (TaskCanceledException ex) {
            // connection timed out
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[1];
                  action();
               });
         }
         catch (Exception ex) {
            // some other error happened
         }
         return _documentList;
      }

      // Gets "My Documents" filtered by API
      public async static Task<ObservableCollection<Document>> getMyDocuments(int start, int limit) {
         ObservableCollection<Document> _documentList = new ObservableCollection<Document>();

         try {
            //Sort parameter allows for the API to return the list in a sorted manner
            HttpResponseMessage result = await client.GetAsync("objects/documents/?" + "start=" + start + "&limit=" + limit + "&sort=" + VeevaAPI.sortParameter
                + "&named_filter=My Documents");
            //Wait for the GET request to finish, then continue to update the GUI
            //parse json results
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            JArray documentList = (JArray)res.documents;
            foreach (JObject obj in documentList) {
               JObject documentObj = (JObject)obj.GetValue("document");
               Document doc = await VeevaAPI.obtainMetadata(documentObj);
               _documentList.Add(doc);
               if (!documentDictionary.ContainsKey(doc.ID))
                  documentDictionary.Add(doc.ID, doc);
            }
         }
         catch (HttpRequestException ex) {
            // no internet
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[0];
                  action();
               });
         }
         catch (TaskCanceledException ex) {
            // connection timed out
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[1];
                  action();
               });
         }
         catch (Exception ex) {
            // some other error happened
         }
         return _documentList;
      }

      // Gets only "Recent Documents"
      public async static Task<ObservableCollection<Document>> getRecentDocuments(int start, int limit) {
         ObservableCollection<Document> _documentList = new ObservableCollection<Document>();
         try {
            //Sort parameter allows for the API to return the list in a sorted manner, currently sorted by name in descending order
            HttpResponseMessage result = await client.GetAsync("objects/documents/?" + "start=" + start + "&limit=" + limit + "&sort=" + VeevaAPI.sortParameter
                + "&named_filter=Recent Documents");
            //Wait for the GET request to finish, then continue to update the GUI
            //parse json results
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            JArray documentList = (JArray)res.documents;
            foreach (JObject obj in documentList) {
               JObject documentObj = (JObject)obj.GetValue("document");
               Document doc = await VeevaAPI.obtainMetadata(documentObj);
               _documentList.Add(doc);
               if (!documentDictionary.ContainsKey(doc.ID))
                  documentDictionary.Add(doc.ID, doc);
            }
         }
         catch (HttpRequestException ex) {
            // no internet
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[0];
                  action();
               });
         }
         catch (TaskCanceledException ex) {
            // connection timed out
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[1];
                  action();
               });
         }
         catch (Exception ex) {
            // some other error happened
         }
         return _documentList;
      }

      // Gets only "Favorites" documents
      public async static Task<ObservableCollection<Document>> getFavoriteDocuments(int start, int limit) {
         ObservableCollection<Document> _documentList = new ObservableCollection<Document>();
         try {
            //Sort parameter allows for the API to return the list in a sorted manner
            HttpResponseMessage result = await client.GetAsync("objects/documents/?" + "start=" + start + "&limit=" + limit + "&sort=" + VeevaAPI.sortParameter
                + "&named_filter=Favorites");
            //Wait for the GET request to finish, then continue to update the GUI
            //parse json results
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            JArray documentList = (JArray)res.documents;
            foreach (JObject obj in documentList) {
               JObject documentObj = (JObject)obj.GetValue("document");
               Document doc = await VeevaAPI.obtainMetadata(documentObj);
               _documentList.Add(doc);
               if (!documentDictionary.ContainsKey(doc.ID))
                  documentDictionary.Add(doc.ID, doc);
            }
         }
         catch (HttpRequestException ex) {
            // no internet connection
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[0];
                  action();
               });
         }
         catch (TaskCanceledException ex) {
            // connection timed out
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[1];
                  action();
               });
         }
         catch (Exception ex) {
            // some other error happened
         }
         return _documentList;
      }

      //GetVideo receives the video rendition for a video inside veeva vault
      public async static Task<StorageFile> GetVideo(string docId, string major, string minor) {
         try {
            HttpResponseMessage result;
            if (major == null) {
               result = await client.GetAsync("objects/documents/" + docId + "/renditions/video_rendition__v");
            }
            else {
               result = await client.GetAsync("objects/documents/" + docId + "/versions/" + major + "/" + minor + "/renditions/video_rendition__v");
            }
            StorageFolder myfolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            // don't use this folder - gives unauthorized access exception
            //StorageFolder myfolder = Windows.ApplicationModel.Package.Current.InstalledLocation; 
            StorageFile sampleFile = await myfolder.CreateFileAsync(docId, CreationCollisionOption.ReplaceExisting);
            byte[] file = await result.Content.ReadAsByteArrayAsync();

            await FileIO.WriteBytesAsync(sampleFile, file); //creates video file
            return sampleFile;
         }
         catch (HttpRequestException ex) {
            // no internet
            return null;
         }
         catch (TaskCanceledException ex) {
            // connection timed out
            return null;
         }
         catch (Exception ex) {
            // some other error happened
            return null;
         }
      }

      //RenderDocument gets called from DocumentViewModel, currenly only PDFs get rendered correctly
      //This function also calls helper method renderPDF
      public async static Task<ObservableCollection<Image>> renderDocument(string docId, string major, string minor) {

         try {
            //viewable renditions allow us to load images and text documents to be viewed as pdfs
            HttpResponseMessage result;
            if (major == null) {
               result = await client.GetAsync("objects/documents/" + docId + "/renditions/viewable_rendition__v");
            }
            else {
               result = await client.GetAsync("objects/documents/" + docId + "/versions/" + major + "/" + minor + "/renditions/viewable_rendition__v");
            }
            StorageFolder myfolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            // don't use this folder - gives unauthorized access exception
            //StorageFolder myfolder = Windows.ApplicationModel.Package.Current.InstalledLocation; 
            StorageFile sampleFile = await myfolder.CreateFileAsync(docId, CreationCollisionOption.ReplaceExisting);
            byte[] file = await result.Content.ReadAsByteArrayAsync();

            await FileIO.WriteBytesAsync(sampleFile, file); // creates pdf file that was retrieved

            ObservableCollection<Image> docPDF = await VeevaAPI.RenderPDF(myfolder, docId);
            //docPDF could be null after the return from the helper method
            if (docPDF != null) {
               return docPDF;
            }
            else {
               //placeholder found 
               if (documentDictionary[docId].Format.Equals("")) {
                  //do not crash but return an empty collection
                  return new ObservableCollection<Image>();
               }
               Debug.WriteLine("ERROR: Failed inside VeevaAPI.renderDocument");
            }

         }
         catch (HttpRequestException ex) {
            // no internet
         }
         catch (TaskCanceledException ex) {
            // connection timed out
         }
         catch (Exception ex) {
            // some other error happened
         }
         return null;
      }

      // try to merge this into obtainMetadata() later
      public async static Task<ObservableCollection<string>> GetDocumentVersions(string docId) {
         try {
            HttpResponseMessage result = await client.GetAsync("objects/documents/" + docId + "/versions");
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JArray versions = (JArray)res.versions;

            List<string> keys = new List<string>();
            for (int i = 0; i < versions.Count; i++) {
               JObject obj = (JObject)versions[i];
               keys.Add(obj.GetValue("number").ToString());
            }

            keys.Sort(); // change this later so that it sorts in descending order
            return new ObservableCollection<string>(keys);
         }
         catch (HttpRequestException ex) {
            // no internet
         }
         catch (TaskCanceledException ex) {
            // connection timed out
         }
         catch (Exception ex) {
            // some other error happened
         }
         return new ObservableCollection<string>();
      }

      private static async Task<Document> obtainMetadata(JObject documentObj) {
         string id = documentObj.GetValue("id").ToString() == null ? "" : documentObj.GetValue("id").ToString();
         string title = documentObj.GetValue("title__v") == null ? "" : documentObj.GetValue("title__v").ToString();
         string docName = documentObj.GetValue("name__v").ToString() == null ? "" : documentObj.GetValue("name__v").ToString();
         string docType = documentObj.GetValue("type__v") == null ? "" : documentObj.GetValue("type__v").ToString();
         string format = documentObj.GetValue("format__v") == null ? "" : documentObj.GetValue("format__v").ToString();
         string docProductType = "";
         foreach (String product in documentObj.GetValue("product__v")) {
            docProductType += product + "\n";
         }
         JObject ownerObj = (JObject)documentObj.GetValue("owner__v");
         VaultUser owner = await GetUser(ownerObj.GetValue("users").First.ToString());

         VaultUser lastModifiedBy = await GetUser(documentObj.GetValue("document_last_modified_by__v").ToString());
         string major = documentObj.GetValue("major_version_number__v") == null ? "" : documentObj.GetValue("major_version_number__v").ToString();
         string minor = documentObj.GetValue("minor_version_number__v") == null ? "" : documentObj.GetValue("minor_version_number__v").ToString();
         bool isBinder = Convert.ToBoolean(documentObj.GetValue("binder__v").ToString());

         VaultUser createdBy = await GetUser(documentObj.GetValue("created_by__v").ToString());
         string documentNumber = documentObj.GetValue("document_number__v").ToString();
         string version = major + "." + minor;
         string lifecycle = documentObj.GetValue("lifecycle__v").ToString();
         string status = documentObj.GetValue("status__v").ToString();

         string modifyDate = documentObj.GetValue("version_modified_date__v").ToString();
         string creationDate = documentObj.GetValue("version_creation_date__v").ToString();

         ShareSetting reviewers = buildShareSetting((JObject)documentObj.GetValue("reviewer__v"));
         reviewers.Name = "reviewer__v";
         ShareSetting viewers = buildShareSetting((JObject)documentObj.GetValue("viewer__v"));
         viewers.Name = "viewer__v";
         ShareSetting consumers = buildShareSetting((JObject)documentObj.GetValue("consumer__v"));
         consumers.Name = "consumer__v";
         ShareSetting approvers = buildShareSetting((JObject)documentObj.GetValue("approver__v"));
         approvers.Name = "approver__v";
         ShareSetting editors = buildShareSetting((JObject)documentObj.GetValue("editor__v"));
         editors.Name = "editor__v";

         BitmapImage bitmap = null;
         try {
            // get the thumbnail
            HttpResponseMessage response = await client.GetAsync("objects/documents/" + id + "/versions/" + major + "/" + minor + "/thumbnail");
            response.EnsureSuccessStatusCode();
            Stream stream = await response.Content.ReadAsStreamAsync();
            // because bitmap.SetSource takes an IRandomAccessStream and there's no easy way to convert a
            // Stream to that. 
            var memStream = new MemoryStream();
            await stream.CopyToAsync(memStream);
            memStream.Position = 0;

            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                CoreDispatcherPriority.Normal, () => {
                   bitmap = new BitmapImage();
                   bitmap.SetSource(memStream.AsRandomAccessStream());
                });
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         Document doc = new Document(docName, title, docProductType, isBinder, docType, lastModifiedBy,
             bitmap, id, documentNumber, format, createdBy, version, lifecycle, status, owner, null, modifyDate, creationDate,
             reviewers, viewers, consumers, approvers, editors);
         return doc;
      }

      private static ShareSetting buildShareSetting(JObject setting) {
         JArray arr = (JArray)setting.GetValue("users");
         List<string> users = new List<string>();
         foreach (string user in arr) {
            users.Add(user);
         }

         arr = (JArray)setting.GetValue("groups");
         List<string> groups = new List<string>();
         foreach (string group in arr) {
            groups.Add(group);
         }

         return new ShareSetting("", users, groups);
      }

      public static async Task<List<Document>> GetSupportingDocuments(string docId, string major, string minor) {
         try {
            HttpResponseMessage result = await client.GetAsync("objects/documents/" + docId + "/versions/" + major + "/" + minor +"/relationships");
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            List<Document> docIds = new List<Document>();
            JArray relationships = (JArray)res.relationships;
            if (relationships != null) {
               foreach (JObject rel in relationships) {
                  JObject relationship = (JObject)rel.GetValue("relationship");
                  string relType = relationship.GetValue("relationship_type__v").ToString();
                  if (relType.Equals("supporting_documents__vs")) {
                     string id = relationship.GetValue("target_doc_id__v").ToString();
                     Document doc;
                     if (documentDictionary.ContainsKey(id)) {
                        doc = documentDictionary[id];
                     }
                     else {
                        doc = await GetDocumentById(id);
                     }
                     docIds.Add(doc);
                  }
               }
            }
            return docIds;
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         
         return new List<Document>();
      }

      //Helper method that returns and image of the pdf document
      private static async Task<ObservableCollection<Image>> RenderPDF(StorageFolder folder, string filename) {
         ObservableCollection<Image> pdfImages = new ObservableCollection<Image>();
         try {
            StorageFile doc = await folder.GetFileAsync(filename);
            PdfDocument pdf = await PdfDocument.LoadFromFileAsync(doc); // this line throws the exception

            for (uint ndx = 0; ndx < pdf.PageCount; ndx++) {
               var pdfPage = pdf.GetPage(ndx);

               if (pdfPage != null) {
                  StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;

                  StorageFile jpgFile = await tempFolder.CreateFileAsync(Guid.NewGuid().ToString() +
                      ".png", CreationCollisionOption.ReplaceExisting);
                  if (jpgFile != null) {
                     IRandomAccessStream randomStream = await jpgFile.OpenAsync(FileAccessMode.ReadWrite);

                     PdfPageRenderOptions pdfPageRenderOptions = new PdfPageRenderOptions();
                     //Render Pdf page with default options 
                     await pdfPage.RenderToStreamAsync(randomStream);

                     await randomStream.FlushAsync();

                     // free resources
                     randomStream.Dispose();
                     pdfPage.Dispose();

                     // create UI image
                     Image image = new Image();
                     BitmapImage temp = new BitmapImage();
                     temp.SetSource(await jpgFile.OpenAsync(FileAccessMode.Read));
                     image.Source = temp;
                     image.Stretch = Stretch.None;
                     image.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;

                     pdfImages.Add(image);
                  }
               }
            }
            return pdfImages;
         }
         catch (Exception ex) {
            Debug.WriteLine("ERROR: Failed inside of catch block in VeevaAPI.RenderPDF, exception caught");
            return null;
         }
      }

      //Retrieves list of downloaded documents from the model
      public static async Task<List<Document>> getDownloadList() {
         List<Document> downloadList = new List<Document>();

         StorageFolder downloadsFolder = null;

         // look for downloads token in future access list
         if (StorageApplicationPermissions.FutureAccessList.Entries.Any(item => item.Metadata == "Veeva Downloads")) {

            // if we have the folder in the FutureAccessList, just ui
            var entry = StorageApplicationPermissions.FutureAccessList.Entries.FirstOrDefault(item => item.Metadata == "Veeva Downloads");
            try {
               downloadsFolder = await StorageApplicationPermissions.FutureAccessList.GetFolderAsync(entry.Token);
            }
            catch (Exception ex) {
               // downloads folder was deleted, delete this token.
               StorageApplicationPermissions.FutureAccessList.Remove(entry.Token);
               return null;
            }
         }
         else {
            downloadsFolder = await DownloadsFolder.CreateFolderAsync("Veeva Downloads");
            StorageApplicationPermissions.FutureAccessList.Add(downloadsFolder, "Veeva Downloads");
         }

         if (downloadsFolder != null) {
            IReadOnlyList<StorageFile> fileList = await downloadsFolder.GetFilesAsync();

            foreach (StorageFile file in fileList) {
               // add file to downloadList
               downloadList.Add(new Document(file.Name, file.FileType.ToString()));
            }
         }
         else {
            Debug.WriteLine("ERROR: Downloads folder");
         }

         return downloadList;
      }

      // Called by DocumentListViewModel to show PDF of a document in Document List page
      public static async Task<ObservableCollection<Image>> renderDownloadedDocument(string fileName) {

         StorageFolder downloadsFolder = null;

         if (StorageApplicationPermissions.FutureAccessList.Entries.Any(item => item.Metadata == "Veeva Downloads")) {
            // if we have the folder in the FutureAccessList, just ui
            var entry = StorageApplicationPermissions.FutureAccessList.Entries.FirstOrDefault(item => item.Metadata == "Veeva Downloads");
            try {
               downloadsFolder = await StorageApplicationPermissions.FutureAccessList.GetFolderAsync(entry.Token);
            }
            catch (Exception ex) {
               // downloads folder was deleted, delete this token.
               StorageApplicationPermissions.FutureAccessList.Remove(entry.Token);
               return null;
            }
         }
         else {
            downloadsFolder = await DownloadsFolder.CreateFolderAsync("Veeva Downloads");
            StorageApplicationPermissions.FutureAccessList.Add(downloadsFolder, "Veeva Downloads");
         }

         if (downloadsFolder != null) {
            ObservableCollection<Image> docPDF = await VeevaAPI.RenderPDF(downloadsFolder, fileName);
            if (docPDF != null) {
               return docPDF;
            }
            else {
               Debug.WriteLine("ERROR: Failed inside VeevaAPI.renderDownloadedDocument");
               return null;
            }
         }
         else {
            Debug.WriteLine("ERROR: Downloads folder");
         }

         return null;
      }

      // Called by DocumentListViewModel to show video of document in downloaded list page
      public static async Task<StorageFile> renderDownloadedVideo(string fileName) {

         StorageFolder downloadsFolder = null;

         if (StorageApplicationPermissions.FutureAccessList.Entries.Any(item => item.Metadata == "Veeva Downloads")) {
            // if we have the folder in the FutureAccessList, just ui
            var entry = StorageApplicationPermissions.FutureAccessList.Entries.FirstOrDefault(item => item.Metadata == "Veeva Downloads");
            try {
               downloadsFolder = await StorageApplicationPermissions.FutureAccessList.GetFolderAsync(entry.Token);
            }
            catch (Exception ex) {
               // downloads folder was deleted, delete this token.
               StorageApplicationPermissions.FutureAccessList.Remove(entry.Token);
               return null;
            }
         }
         else {
            downloadsFolder = await DownloadsFolder.CreateFolderAsync("Veeva Downloads");
            StorageApplicationPermissions.FutureAccessList.Add(downloadsFolder, "Veeva Downloads");
         }

         if (downloadsFolder != null) {
            StorageFile video = await downloadsFolder.GetFileAsync(fileName);
            if (video != null) {
               return video;
            }
            else {
               Debug.WriteLine("ERROR: Couldn't retrieve video from downloads folder");
            }
         }
         else {
            Debug.WriteLine("ERROR: Downloads folder, videos");
         }

         return null;
      }

      // Called by DocumentListViewModel to show image in downloaded list page
      public static async Task<Image> renderDownloadedImage(string fileName) {

         StorageFolder downloadsFolder = null;

         if (StorageApplicationPermissions.FutureAccessList.Entries.Any(item => item.Metadata == "Veeva Downloads")) {
            // if we have the folder in the FutureAccessList, just ui
            var entry = StorageApplicationPermissions.FutureAccessList.Entries.FirstOrDefault(item => item.Metadata == "Veeva Downloads");
            try {
               downloadsFolder = await StorageApplicationPermissions.FutureAccessList.GetFolderAsync(entry.Token);
            }
            catch (Exception ex) {
               // downloads folder was deleted, delete this token.
               StorageApplicationPermissions.FutureAccessList.Remove(entry.Token);
               return null;
            }
         }
         else {
            downloadsFolder = await DownloadsFolder.CreateFolderAsync("Veeva Downloads");
            StorageApplicationPermissions.FutureAccessList.Add(downloadsFolder, "Veeva Downloads");
         }

         if (downloadsFolder != null) {
            StorageFile imageFile = await downloadsFolder.GetFileAsync(fileName);
            Image image = new Image();
            BitmapImage temp = new BitmapImage();
            temp.SetSource(await imageFile.OpenAsync(FileAccessMode.Read));
            image.Source = temp;
            return image;
         }
         else {
            Debug.WriteLine("ERROR: Downloads folder, images");
         }

         return null;
      }

      //DownloadDocument downsload the specified document based off the ID passed in
      public static async Task<byte[]> DownloadDocument(string docId) {
         // See Downloading a File: http://msdn.microsoft.com/en-us/library/windows/apps/jj152726.aspx
         try {
            // Retrieve Document File veeva vault API call 
            HttpResponseMessage response = await client.GetAsync("objects/documents/" + docId + "/file");

            // get document data from HttpResponseMessage.Content
            byte[] data = await response.Content.ReadAsByteArrayAsync();

            StorageFile myFile;

            StorageFolder downloadsFolder = null;

            if (StorageApplicationPermissions.FutureAccessList.Entries.Any(item => item.Metadata == "Veeva Downloads")) {
               // if we have the folder in the FutureAccessList, just ui
               var entry = StorageApplicationPermissions.FutureAccessList.Entries.FirstOrDefault(item => item.Metadata == "Veeva Downloads");
               try {
                  downloadsFolder = await StorageApplicationPermissions.FutureAccessList.GetFolderAsync(entry.Token);
               }
               catch (Exception ex) {
                  // downloads folder was deleted, delete this token.
                  StorageApplicationPermissions.FutureAccessList.Remove(entry.Token);
                  return null;
               }
            }
            else {
               downloadsFolder = await DownloadsFolder.CreateFolderAsync("Veeva Downloads");
               StorageApplicationPermissions.FutureAccessList.Add(downloadsFolder, "Veeva Downloads");
            }

            if (downloadsFolder != null) {
               //save video as mp4
               if (documentDictionary[docId].Format.Contains("video")) {
                  myFile = await downloadsFolder.CreateFileAsync(documentDictionary[docId].DocumentName + ".mp4");
               }
               else if (documentDictionary[docId].Format.Contains("image")) {
                  myFile = await downloadsFolder.CreateFileAsync(documentDictionary[docId].DocumentName + ".png");
               }
               else {
                  //save documents and images as pdfs
                  // will show .pdf.pdf if the doc name has .pdf in it.
                  if (documentDictionary[docId].DocumentName.Contains(".pdf")) {
                     myFile = await downloadsFolder.CreateFileAsync(documentDictionary[docId].DocumentName);
                  }
                  else {
                     myFile = await downloadsFolder.CreateFileAsync(documentDictionary[docId].DocumentName + ".pdf");
                  }
               }

               //Write File data to the file
               await FileIO.WriteBytesAsync(myFile, data);

               // Go to your Downloads > WinVault folder to view!
               return data;
            }
            else {
               Debug.WriteLine("ERROR: Downloads folder");
               return null;
            }
         }
         catch (HttpRequestException ex) {
            return null;
         }
         catch (TaskCanceledException ex) {
            return null;
         }
         catch (Exception ex) {
            // Show message dialog
            Debug.WriteLine("Exception: " + ex.Message);
            return null;
         }
      }

      //returns the value from the docID string
      public async static Task<Document> GetDocumentProperties(String docId) {
         if (documentDictionary.ContainsKey(docId)) {
            return documentDictionary[docId];
         }
         else {
            try {
               HttpResponseMessage result = await client.GetAsync("objects/documents/" + docId);

               //parse json results
               dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
               JObject documentObj = (JObject)res.document;
               Document doc = await obtainMetadata(documentObj);
               documentDictionary.Add(docId, doc);
               return documentDictionary[docId];
            }
            catch (HttpRequestException ex) {
               // no internet connection
            }
            catch (TaskCanceledException ex) {
               // connection timed out
            }
            catch (Exception ex) {
               // some other error happened
            }
            return null;
         }
      }

      public async static Task<Document> GetDocumentVersionProperties(string docId, string major, string minor) {
         try {
            HttpResponseMessage result = await client.GetAsync("objects/documents/" + docId + "/versions/" + major + "/" + minor);

            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JObject documentObj = (JObject)res.document;
            Document doc = await obtainMetadata(documentObj);
            doc.ID = docId + major + minor; // don't override current version
            documentDictionary.Add(doc.ID, doc);
            return documentDictionary[doc.ID];
         }
         catch (HttpRequestException ex) {
            // no internet
         }
         catch (TaskCanceledException ex) {
            // connection timed out
         }
         catch (Exception ex) {
            // some other error happened
         }
         return null;

      }

      public async static Task<JObject> GetDocumentRenditions(string docId) {
         try {
            HttpResponseMessage result = await client.GetAsync("objects/documents/" + docId);

            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JObject renditions = (JObject)res.renditions;
            return renditions;
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {
            // some other error happened
         }
         return null;
      }

      public async static Task<VaultUser> GetUser(string userId) {
         //check if we have already processed user
         if (userDictionary.ContainsKey(userId))
            return userDictionary[userId];

         try {
            HttpResponseMessage result = await client.GetAsync("objects/users/" + userId);
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JArray user = res.users;
            VaultUser vaultUser = new VaultUser();

            //only 1 iteration
            foreach (JObject obj in user) {
               JObject userObj = (JObject)obj.GetValue("user");
               vaultUser.Id = Convert.ToInt32(userId);
               vaultUser.Username = userObj.GetValue("user_name__v").ToString();
               vaultUser.FirstName = userObj.GetValue("user_first_name__v").ToString();
               vaultUser.LastName = userObj.GetValue("user_last_name__v").ToString();
            }
            userDictionary.Add(userId, vaultUser);
            return vaultUser;
         }
         catch (HttpRequestException ex) {
            // no internet
         }
         catch (TaskCanceledException ex) {
            // connection timed out
         }
         catch (Exception ex) {
            // some other error happened
         }
         return null;
      }

      // GetMyTask gets a single task, called from MyTaskDetailViewModel to show a single task
      // Need to retrieve All Fields, unlike GetMyTasks
      public async static Task<MyTask> GetMyTask(string taskID) {
         try {
            HttpResponseMessage result = await client.GetAsync(Query.Task(taskID));
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JArray taskList = (JArray)res.resultSet;

            if (taskList != null) {
               foreach (JObject obj in taskList) {
                  JArray task = (JArray)obj.GetValue("values");
                  // only get tasks assigned to this user
                  string docName = await GetDocumentNameById(task[14].ToString()); // Doc ID
                  return new MyTask(task[0].ToString(), // TaskID
                          task[1].ToString(), // TaskName
                          task[2].ToString(), // TaskAssigneeID
                          task[3].ToString(), // Assignee Name
                          task[4].ToString(), // TaskStatus
                          task[5].ToString(), // TaskAssignmentDate
                          task[6].ToString(), // TaskCancellationDate
                          task[7].ToString(), // TaskCompletionDate
                          task[8].ToString(), // TaskCreationDate
                          task[9].ToString(), // TaskDueDate
                          task[10].ToString(), // TaskDuration
                          task[11].ToString(), // TaskQueueGroup
                          task[12].ToString(), // TaskVerdict
                          task[13].ToString(),
                          task[14].ToString(), // Document ID
                          docName); // Document Title

               }
            }
         }
         catch (HttpRequestException ex) {
            // no internet
         }
         catch (TaskCanceledException ex) {
            // connection timed out
         }
         catch (Exception ex) {
            // some other error happened
         }
         return null;
      }

      // Retrieve the necessary information for home page tasks
      public async static Task<ObservableCollection<MyTask>> GetMyTasks(int start, int limit) {
         ObservableCollection<MyTask> list = new ObservableCollection<MyTask>();
         try {
            HttpResponseMessage result = await client.GetAsync(Query.MyTasks);
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JArray taskList = (JArray)res.resultSet;

            if (taskList != null) {
               foreach (JObject obj in taskList) {
                  JArray task = (JArray)obj.GetValue("values");
                  // Home Page only needs task id, task name, task assignee, task status, task assignment date, task due date, task document ID
                  string docName = await GetDocumentNameById(task[6].ToString()); // Doc ID
                     list.Add(new MyTask(task[0].ToString(), // TaskID
                                 task[1].ToString(), // TaskName
                                 task[2].ToString(), // Assignee Name
                                 task[3].ToString(), // TaskStatus
                                 task[4].ToString(), // TaskAssignmentDate
                                 task[5].ToString(), // TaskDueDate
                                 task[6].ToString(), // Doc ID
                                 task[7].ToString(), // Task Comments
                                 docName));
               }
            }
         }
         catch (HttpRequestException ex) {
            // no internet
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[0];
                  action();
               });
         }
         catch (TaskCanceledException ex) {
            // connection timed out
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[1];
                  action();
               });
         }
         catch (Exception ex) {
            // other error
         }
         return list;
      }

      // Get tasks associated with an active workflow
      public async static Task<ObservableCollection<MyTask>> GetActiveWorkflowTasks(string workflowID) {

         ObservableCollection<MyTask> taskList = new ObservableCollection<MyTask>();
         try {
            HttpResponseMessage result = await client.GetAsync(Query.ActiveWorkflowTasks(workflowID));
            String strRes = await result.Content.ReadAsStringAsync();
            if (strRes.Contains("html exception"))
               return taskList;

            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JArray workflowList = (JArray)res.resultSet;
            if (workflowList != null) {
               foreach (JObject obj in workflowList) {
                  JArray workflow = (JArray)obj.GetValue("values");

                  // home page only needs id, document id, name, status, dueDate.
                  taskList.Add(new MyTask(workflow[1].ToString(), // Task Name
                                        workflow[2].ToString(), // Task Assignee Name
                                        workflow[3].ToString(),
                                        workflow[4].ToString())); // Task Due Date

               }
            }
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return taskList;
      }

      public async static Task<ActiveWorkflow> GetActiveWorkflowByDocumentID(string docID) {
         try {
            HttpResponseMessage result = await client.GetAsync(Query.ActiveWorkflowByDocumentID(docID));
            String strRes = await result.Content.ReadAsStringAsync();
            if (strRes.Contains("html exception"))
               return null;

            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JArray workflowList = (JArray)res.resultSet;
            if (workflowList != null) {
               foreach (JObject obj in workflowList) {
                  JArray workflow = (JArray)obj.GetValue("values");
                  string docName = await GetDocumentNameById(workflow[1].ToString()); // Doc ID

                  // home page only needs id, document id, name, status, dueDate.
                  return new ActiveWorkflow(workflow[0].ToString(), // WorkflowID
                                        workflow[1].ToString(), // WorkflowDocID
                                        workflow[2].ToString(), // WorkflowName
                                        workflow[3].ToString(), // workflow initiator name
                                        workflow[4].ToString(), // WorkflowStatus
                                        workflow[5].ToString(),// WorkflowDueDate
                                        docName);

               }
            }
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return null;
      }

      public async static Task<ObservableCollection<ActiveWorkflow>> GetActiveWorkflows(int start, int limit) {

         ObservableCollection<ActiveWorkflow> activeWorkflowsList = new ObservableCollection<ActiveWorkflow>();
         try {
            HttpResponseMessage result = await client.GetAsync(Query.MyActiveWorkflows);
            String strRes = await result.Content.ReadAsStringAsync();
            if (strRes.Contains("html exception"))
               return activeWorkflowsList;

            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            JArray workflowList = (JArray)res.resultSet;
            if (workflowList != null) {
               foreach (JObject obj in workflowList) {
                  JArray workflow = (JArray)obj.GetValue("values");
                  string docName = await GetDocumentNameById(workflow[1].ToString()); // Doc ID
                  if (docName != null) {
                     // home page only needs id, document id, name, status, dueDate.
                     activeWorkflowsList.Add(new ActiveWorkflow(workflow[0].ToString(), // WorkflowID
                                           workflow[1].ToString(), // WorkflowDocID
                                           workflow[2].ToString(), // WorkflowName
                                           workflow[3].ToString(), // workflow initiator name
                                           workflow[4].ToString(), // WorkflowStatus
                                           workflow[5].ToString(),// WorkflowDueDate
                                           docName)); 
                  }
               }
            }
         }
         catch (HttpRequestException ex) {
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[0];
                  action(); 
               });

         }
         catch (TaskCanceledException ex) {
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[1];
                  action();
            });
         }
         catch (Exception ex) {

         }
         return activeWorkflowsList;
      }

      // updates one document property
      public static Task<HttpResponseMessage> UpdateDocumentProperty(string docId, string propertyName, string value) {
         var content = new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>(propertyName, value),
                });
         // returns the doc id if it succeeded
         try {
            Task<HttpResponseMessage> result = client.PutAsync("objects/documents/" + docId, content);
            return result;
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return null; 
      }

      // gets the metadata each property with the matching type (I don't think there's a way to 
      // only get metadata for a single property) 
      // gets queryable, editable, etc. stuff for each property
      public async static Task GetPropertiesMetadata() {
         try {
            HttpResponseMessage result = await client.GetAsync("metadata/objects/documents/properties");
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            List<DocumentPropertyMetadata> metaList = new List<DocumentPropertyMetadata>();

            JArray properties = (JArray)res.properties;
            foreach (JObject obj in properties) {
               //skip first item as it does not contain any useful information
               if (obj.GetValue("section") == null)
                  continue;

               string name = obj.GetValue("name").ToString();
               string section = obj.GetValue("section").ToString();
               string label = obj.GetValue("label").ToString(); // id doesn't have label, so will be null
               bool required = Convert.ToBoolean(obj.GetValue("required").ToString());
               bool repeat = Convert.ToBoolean(obj.GetValue("repeating").ToString());
               bool edit = Convert.ToBoolean(obj.GetValue("editable").ToString());
               bool hidden = Convert.ToBoolean(obj.GetValue("hidden").ToString());
               bool query = Convert.ToBoolean(obj.GetValue("queryable").ToString());

               DocumentPropertyMetadata meta = new DocumentPropertyMetadata(name, section, label, required, repeat, edit, hidden, query);
               metaList.Add(meta);
            }

            User user = User.Instance;
            foreach (Vault vault in user.Vaults) {
               if (user.VaultId == vault.Id) {
                  vault.PropertyMetadata = metaList;
                  break;
               }
            }
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
      }

      // Does keyword search across all document properties, returns a simple document object
      public async static Task<ObservableCollection<Document>> SearchVault(int start, int limit) {
         ObservableCollection<Document> list = new ObservableCollection<Document>();
         try {
            HttpResponseMessage result = await client.GetAsync("objects/documents?search=" + SearchTerm + "&scope=all&start=" + start + "&limit=" + limit + "&sort=name__v desc");
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            JArray documentList = (JArray)res.documents;
            foreach (JObject obj in documentList) {
               //Document doc = new Document();
               JObject documentObj = (JObject)obj.GetValue("document");
               Document doc = await VeevaAPI.obtainMetadata(documentObj);
               list.Add(doc);
               if (!documentDictionary.ContainsKey(doc.ID))
                  documentDictionary.Add(doc.ID, doc);
            }
         }
         catch (HttpRequestException ex) {
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[0];
                  action();
               });
         }
         catch (TaskCanceledException ex) {
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
               CoreDispatcherPriority.Normal, () => {
                  Action action = ErrorHandlingDelegates[1];
                  action();
               });
         }
         catch (Exception ex) {

         }
         return list;
      }

      // Gets number of MyTasks currently assigned to the logged in user.
      // Tried to retrieve with query: SELECT COUNT(DISTINCT task_id__v)
      // but that doesn't work.
      public async static Task<int> GetMyTasksCount() {
         try {
            HttpResponseMessage result = await client.GetAsync(Query.MySimpleTasks);

            String strRes = await result.Content.ReadAsStringAsync();
            if (strRes.Contains("html exception"))
               return 0;

            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            return (int)res.size;
         }
         catch (HttpRequestException ex) {
            System.Diagnostics.Debug.WriteLine("Http exception caught in task count");
         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return 0;
      }

      // Gets number of ActiveWorkflows currently assigned to the logged in user.
      // Tried to retrieve with query: SELECT COUNT(DISTINCT workflow_id__v)
      // but that doesn't work, so this retrieves work flow IDs and counts them up.
      public async static Task<int> GetActiveWorkflowsCount() {
         try {
            HttpResponseMessage result = await client.GetAsync(Query.MySimpleActiveWorkflows); // only get workflows marked "Active"

            String strRes = await result.Content.ReadAsStringAsync();
            if (strRes.Contains("html exception"))
               return 0;

            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);
            return (int)res.size;
         }
         catch (HttpRequestException ex) {
            System.Diagnostics.Debug.WriteLine("Http exception caught in workflow count");                                                                      
         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return 0;
      }

      //return the title of the document based on the document Id passed in
      public static string GetDocumentTitle(string docId) {
         return documentDictionary[docId].DocumentName;
      }

      //Uploads document
      public static bool UploadDocument(HttpContent content) {
         try {
            var result = client.PostAsync("objects/documents", content).Result;
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            return (((String)res.responseStatus).Equals("SUCCESS"));
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {
             
         }
         return false;
      }

      //Get List of Document Types (see http://developer.veevavault.com/docs/api/v7/#DocumentsTypesMetadata)
      public async static Task SetDocumentTypes() {
         documentTypes = new List<string>();
         HttpResponseMessage result = await client.GetAsync("metadata/objects/documents/types");
         dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

         JArray types = (JArray)res.types;
         if (types != null) {
            foreach (JObject type in types) {
               documentTypes.Add(type.GetValue("label").ToString());
            }
         }
      }

      public static List<String> GetDocumentTypes() {
         return documentTypes;
      }

      //Get List of Document Products
      public async static Task SetDocumentProducts() {
         documentProducts = new Dictionary<string, string>();
         HttpResponseMessage result = await client.GetAsync("objects/catalogs/product__v");
         dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

         JArray products = (JArray)res.catalogValues;
         if (products != null) {
            foreach (JObject product in products) {
               JObject prod = (JObject)product.GetValue("catalogValue");
               documentProducts.Add(prod.GetValue("id").ToString(), prod.GetValue("product_name__v").ToString());
            }
         }
      }

      public static List<String> GetProductTypes() {
         return documentProducts.Values.ToList();
      }

      public static string GetProductId(string val) {
         return documentProducts.First(x => x.Value.Equals(val)).Key;
      }

      public static ObservableCollection<Document> sortLibrary(string sortParameter) {
         List<KeyValuePair<string, Document>> docList = VeevaAPI.documentDictionary.ToList();

         if (sortParameter.Equals("name asc")) {
            docList.Sort(
               delegate(KeyValuePair<string, Document> firstPair,
               KeyValuePair<string, Document> nextPair) {
                  return firstPair.Value.DocumentName.CompareTo(nextPair.Value.DocumentName);
               }
            );
         }
         else if (sortParameter.Equals("name desc")) {
            docList.Sort(
               delegate(KeyValuePair<string, Document> firstPair,
               KeyValuePair<string, Document> nextPair) {
                  return firstPair.Value.DocumentName.CompareTo(nextPair.Value.DocumentName);
               }
            );
         }
         ObservableCollection<Document> sortedList = new ObservableCollection<Document>();
         foreach (KeyValuePair<string, Document> doc in docList) {
            sortedList.Add(doc.Value);
         }
         return sortedList;
      }

      //translate view based sort selection to API based sort selection
      public static void setSortParameter(string selection) {    
         var res = new Windows.ApplicationModel.Resources.ResourceLoader();

         if (selection.Equals(res.GetString("DocumentNameAscending/content"))) {
            VeevaAPI.sortParameter = "name__v asc";
         }
         else if (selection.Equals(res.GetString("DocumentNameDescending/content"))) {
            VeevaAPI.sortParameter = "name__v desc";
         }
         else if (selection.Equals(res.GetString("DocumentNumberAscending/content"))) {
            VeevaAPI.sortParameter = "document_number__v asc";
         }
         else if (selection.Equals(res.GetString("DocumentNumberDescending/content"))) {
            VeevaAPI.sortParameter = "document_number__v desc";
         }
         else if (selection.Equals(res.GetString("CreationDateNewest/content"))) {
            VeevaAPI.sortParameter = "version_creation_date__v desc";
         }
         else if (selection.Equals(res.GetString("CreationDateOldest/content"))) {
            VeevaAPI.sortParameter = "version_creation_date__v asc";
         }
         else if (selection.Equals(res.GetString("ModifiedDateNewest/content"))) {
            VeevaAPI.sortParameter = "document_modified_date__v desc";
         }
         else if (selection.Equals(res.GetString("ModifiedDateOldest/content"))) {
            VeevaAPI.sortParameter = "document_modified_date__v asc";
         }
         else if (selection.Equals(res.GetString("DocumentTypeAscending/content"))) {
            VeevaAPI.sortParameter = "type__v asc";
         }
         else if (selection.Equals(res.GetString("DocumentTypeDescending/content"))) {
            VeevaAPI.sortParameter = "type__v desc";
         }
      }

      public async static Task<string> GetGroupName(string groupId) {
         try {
            HttpResponseMessage result = await client.GetAsync("objects/groups/" + groupId);
            dynamic res = JObject.Parse(result.Content.ReadAsStringAsync().Result);

            JArray groups = (JArray)res.groups;
            if (groups != null) {
               foreach (JObject group in groups) {
                  JObject obj = (JObject)group.GetValue("group");
                  return obj.GetValue("group_name__v").ToString();
               }
            }
            return "";
         }
         catch (HttpRequestException ex) {

         }
         catch (TaskCanceledException ex) {

         }
         catch (Exception ex) {

         }
         return "";
      }
   }
}