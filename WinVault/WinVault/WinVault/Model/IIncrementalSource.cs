﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//code taken from http://marcominerva.wordpress.com/2013/05/22/implementing-the-isupportincrementalloading-interface-in-a-window-store-app/comment-page-1/
namespace WinVault.Model
{
    //Interface that forces classes that inherit this interface to have a getPagedItems list
    //Can be used for other functions in our app such as MyTask, Notifications, Workflows
    public interface IIncrementalSource<T>
    {
        Task<IEnumerable<T>> GetPagedItems(int pageIndex, int pageSize);
    }

}
