﻿using System.Threading.Tasks;

namespace WinVault.Model {
   public interface IDataService {
      Task<DataItem> GetData();
   }
}