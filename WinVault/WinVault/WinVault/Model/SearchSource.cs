﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinVault.Model {

   public class SearchSource : IIncrementalSource<Document> {

      private ObservableCollection<Document> DocumentList = new ObservableCollection<Document>();

      public SearchSource() {

      }

      //When the user scrolls to the end of the page, this method gets called to get more paged items
      //Each time the GetPagedItems method will do an API call for 5 documents
      public async Task<IEnumerable<Document>> GetPagedItems(int pageIndex, int pageSize) {
         return await Task.Run<IEnumerable<Document>>(async () => {
            //Wait for the documents call to finish
            //pageSize = 5, pageIndex depends on how far the user has scrolled
            ObservableCollection<Document> tempList = await VeevaAPI.SearchVault(pageIndex * pageSize, pageSize);
            foreach (Document tempDoc in tempList) {
               DocumentList.Add(tempDoc);
            }
            //for each document in the list, update the view
            var result = (from doc in DocumentList
                          select doc).Skip(pageIndex * pageSize).Take(pageSize);

            return result;
         });
      }
   }
}
