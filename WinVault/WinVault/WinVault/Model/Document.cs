﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace WinVault.Model {
   /*
    * Document class used to store metadata about a document to display on the library page
    * Does not contain properties that a user can edit from editing metadata
    */
   public class Document {
      public string DocumentName { get; set; }
      public string Title { get; set; }
      public string ProductName { get; set; }
      public string DocumentType { get; set; }
      public VaultUser LastModifiedBy { get; set; }
      public BitmapImage ThumbnailImage { get; set; }
      public string ID { get; set; }
      public bool IsBinder { get; set; }
      public string Number { get; set; }
      public string Format { get; set; }
      public VaultUser CreatedBy { get; set; }
      public string Version { get; set; } // major + . + minor
      public string Lifecycle { get; set; }
      public string Status { get; set; }
      public VaultUser Owner { get; set; }
      public List<IDocumentProperty> Properties { get; set; }
      public BinderElement BinderElementMetadata { get; set; }
      public string CreationDate { get; set; }
      public string ModifyDate { get; set; }
      public string ModifiedByModifiedDate { get; set; }
      public string CreatedByCreatedDate { get; set; }
      public ShareSetting Reviewers { get; set; }
      public ShareSetting Viewers { get; set; }
      public ShareSetting Consumers { get; set; }
      public ShareSetting Approvers { get; set; }
      public ShareSetting Editors { get; set; }

      //Regular document(pdf, word, plain text)
      public Document(string name, string title, string productName, bool binder, string documentType, VaultUser lastModifiedBy,
       BitmapImage thumbnailImage, string id, string number, string format, VaultUser created, string version, string lifecycle,
       string status, VaultUser owner, BinderElement binderMetadata, string modifyDate, string creationDate, ShareSetting reviewers, 
       ShareSetting viewers, ShareSetting consumers, ShareSetting approvers, ShareSetting editors) {
         this.DocumentName = name;
         this.Title = title;
         this.ProductName = productName;
         this.DocumentType = documentType;
         this.LastModifiedBy = lastModifiedBy;
         this.ThumbnailImage = thumbnailImage;
         this.ID = id;
         this.IsBinder = binder;
         this.Number = number;
         this.Format = format;
         this.CreatedBy = created;
         this.Version = version;
         this.Lifecycle = lifecycle;
         this.Status = status.ToUpper();
         this.Owner = owner;
         this.BinderElementMetadata = binderMetadata;

         this.ModifiedByModifiedDate = lastModifiedBy.FullName + " on " + modifyDate.ToString();
         this.CreatedByCreatedDate = CreatedBy.FullName + " on " + creationDate.ToString();

         this.Reviewers = reviewers;
         this.Viewers = viewers;
         this.Consumers = consumers;
         this.Approvers = approvers;
         this.Editors = editors;

         // Format creation date similar to web interface
         DateTime creationDateTime = DateTime.Parse(creationDate);
         var datefmt = new Windows.Globalization.DateTimeFormatting.DateTimeFormatter("{month.full} {day.integer}");
         this.CreationDate = datefmt.Format(creationDateTime);

         // Format modify date
         DateTime modifyDateTime = DateTime.Parse(modifyDate);
         datefmt = new Windows.Globalization.DateTimeFormatting.DateTimeFormatter("{month.full} {day.integer}");
         this.ModifyDate = datefmt.Format(modifyDateTime);
      }


      //constructor used for downloaded documents list, store the name and the documentType.
      //DocumentType in this context is the document file extension. EX: .pdf, .mp4
      public Document(string name, string documentType) { this.DocumentName = name; this.DocumentType = documentType; }

      public Document() { }
   }
}
