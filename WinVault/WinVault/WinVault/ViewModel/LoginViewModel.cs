﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using WinVault.Common;
using WinVault.Model;
using Windows.UI.Popups;

namespace WinVault.ViewModel {
   public class LoginViewModel : ViewModelBase {

      private readonly IDataService _dataService;
      private readonly INavigationService _navigationService;

      private RelayCommand _navigateCommand;
      private Login credentials;

      public delegate void VoidDelegate();
      public VoidDelegate progressBarDelegate;

      public RelayCommand LoginCommand {
         get {
            return new RelayCommand(
                         () => _navigationService.Navigate(typeof(HomePage)));
         }
      }

      private void errorDelegate() {
         var msg = new MessageDialog("No internet connection was detected.");
         msg.Title = "No Internet Connection";
         msg.ShowAsync();
         progressBarDelegate();
      }

      private async void successDelegate(dynamic res) {
         string sessionId = (string)res.sessionId;
         await VeevaAPI.SetUserInformation(credentials, errorDelegate, timeoutDelegate);
         Credentials.Reset();
         _navigationService.Navigate(typeof(HomePage)); //move this - should not navigate if set user failed.
      }

      private void badDelegate() {
         var msg = new MessageDialog("Invalid username or password.");
         msg.Title = "Login Failed";
         msg.ShowAsync();
         progressBarDelegate();
      }

      private void timeoutDelegate() {
         var msg = new MessageDialog("The connection to Vault timed out.");
         msg.Title = "Connection Timed Out";
         msg.ShowAsync();
         progressBarDelegate();
      }

      public async Task HandleLogin() {
         await VeevaAPI.Login(credentials, successDelegate, badDelegate, errorDelegate, timeoutDelegate);
      }

      public LoginViewModel(IDataService dataService, INavigationService navigationService) {
         _dataService = dataService;
         _navigationService = navigationService;
         credentials = new Login();
         Initialize();
      }

      private async Task Initialize() {
         try {
         }
         catch (Exception ex) {
         }
      }

      public Login Credentials {
         get { return credentials; }
         set { credentials = value; }
      }
   }
}