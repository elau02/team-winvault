﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using WinVault.Common;
using WinVault.Model;
using WinVault.ViewModel;
using System.Collections.ObjectModel;
using Windows.Storage;
using Windows.Storage.Search;

namespace WinVault.ViewModel {
   public class DownloadListViewModel : ViewModelBase {

      private readonly IDataService _dataService;
      private readonly INavigationService _navigationService;

      private RelayCommand _navigateCommand;

      private List<Document> _downloadList;

      /// <summary>
      /// Initializes a new instance of the LibraryViewModel class.
      /// </summary>
      public DownloadListViewModel(IDataService dataService, INavigationService navigationService) {
         _dataService = dataService;
         _navigationService = navigationService;
         Initialize();
      }

      private async Task Initialize() {
      }

      public async Task<List<Document>> getDownloadList() {
         return await VeevaAPI.getDownloadList();
      }

      //getDownloadedDocument is called from the download list page when the user selects a document to view
      public async Task<ObservableCollection<Image>> getDownloadedDocument(string fileName) {
         return await VeevaAPI.renderDownloadedDocument(fileName);
      }

      //getDownloadedVideo is called from the download list page when the user selects a video to watch
      public async Task<StorageFile> getDownloadedVideo(string fileName) {
         return await VeevaAPI.renderDownloadedVideo(fileName);
      }

      //getDownloadedVideo is called from the download list page when the user selects an image to view
      public async Task<Image> getDownloadedImage(string fileName) {
         return await VeevaAPI.renderDownloadedImage(fileName);
      }
   }
}
