﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using WinVault.Common;
using WinVault.Model;
using System.Diagnostics;
using Windows.UI.Popups;

namespace WinVault.ViewModel {

   public class MyTasksViewModel : ViewModelBase {

      private readonly IDataService _dataService;
      private readonly INavigationService _navigationService;

      private RelayCommand _navigateCommand;

      /// <summary>
      /// Initializes a new instance of the LibraryViewModel class.
      /// </summary>
      public MyTasksViewModel(IDataService dataService, INavigationService navigationService) {
         _dataService = dataService;
         _navigationService = navigationService;
         Initialize();
      }

      private async Task Initialize() {
         try {

         }
         catch (Exception ex) {
         }
      }

      public delegate void VoidDelegate();
      public VoidDelegate MTProgressBar { get; set; }

      private void errorDelegate() {
         var msg = new MessageDialog("No internet connection was detected.");
         msg.Title = "No Internet Connection";
         msg.ShowAsync();
         MTProgressBar();
      }

      private void timeoutDelegate() {
         var msg = new MessageDialog("The connection timed out. Vault might be down.");
         msg.Title = "Connection Timed Out";
         msg.ShowAsync();
         MTProgressBar();
      }
      //This method will create a new incremental loading collection class which then does an API call for the documents
      //FLOW: View, onNavigatedTo --> ViewModel getMyTasksList --> Model IncrementalLoadingCollection class
      public IncrementalLoadingCollection<MyTaskSource, MyTask> getMyTasksList(Action action) {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;

         return new IncrementalLoadingCollection<MyTaskSource, MyTask>(action);
      }
   }
}