﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using WinVault.Common;
using WinVault.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Popups;

namespace WinVault.ViewModel {
   /// <summary>
   /// This class contains properties that the main View can data bind to.
   /// <para>
   /// See http://www.galasoft.ch/mvvm
   /// </para>
   /// </summary>
   public class HomeViewModel : ViewModelBase {

      private readonly IDataService _dataService;
      private readonly INavigationService _navigationService;

      private RelayCommand _navigateCommand;

      private int _totalMyTasks;
      private int _totalActiveWorkflows;

      public int TotalMyTasks {
         get { return _totalMyTasks; }
         set { _totalMyTasks = value; }
      }

      public int TotalActiveWorkflows {
         get { return _totalActiveWorkflows; }
         set { _totalActiveWorkflows = value; }
      }

      /// <summary>
      /// Goes to My Tasks page
      /// </summary>
      public RelayCommand MyTasksCommand {
         get {
            return new RelayCommand(
                       () => _navigationService.Navigate(typeof(MyTasksPage)));
         }
      }

      /// <summary>
      /// Goes to Active Workflows page
      /// </summary>
      public RelayCommand ActiveWorkflowsCommand {
         get {
            return new RelayCommand(
                       () => _navigationService.Navigate(typeof(ActiveWorkflowsPage)));
         }
      }


      private ObservableCollection<MyTask> _myTasks;
      public ObservableCollection<MyTask> MyTasks {
         get { return _myTasks; }
         set { _myTasks = value; }
      }

      private ObservableCollection<ActiveWorkflow> _activeWorkflows;
      public ObservableCollection<ActiveWorkflow> ActiveWorkflows {
         get { return _activeWorkflows; }
         set { _activeWorkflows = value; }
      }

      public delegate void VoidDelegate();

      /// <summary>
      /// Initializes a new instance of the HomeViewModel class.
      /// </summary>
      public HomeViewModel(IDataService dataService, INavigationService navigationService) {
         _dataService = dataService;
         _navigationService = navigationService;
         MyTasks = new ObservableCollection<MyTask>();
         ActiveWorkflows = new ObservableCollection<ActiveWorkflow>();
         Initialize();
      }

      public VoidDelegate MTProgressBar;
      public VoidDelegate AWProgressBar;

      private async Task Initialize() {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;
         try {
            TotalMyTasks = await VeevaAPI.GetMyTasksCount();
            TotalActiveWorkflows = await VeevaAPI.GetActiveWorkflowsCount();
            MyTasks = await VeevaAPI.GetMyTasks(0, 4);
            ActiveWorkflows = await VeevaAPI.GetActiveWorkflows(0, 4);
         }
         catch (Exception ex) {
            // Report error here
         }
      }

      private void errorDelegate() {
         var msg = new MessageDialog("No internet connection was detected.");
         msg.Title = "No Internet Connection";
         msg.ShowAsync();
         MTProgressBar();
         AWProgressBar();
      }

      private void timeoutDelegate() {
         var msg = new MessageDialog("The connection timed out. Vault might be down.");
         msg.Title = "Connection Timed Out";
         msg.ShowAsync();
         MTProgressBar();
         AWProgressBar();
      }

      public IncrementalLoadingCollection<MyTaskSource, MyTask> getMyTasksList(Action DoneLoadingTasks) {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;
         return new IncrementalLoadingCollection<MyTaskSource, MyTask>(DoneLoadingTasks, 2, 8);
      }

      public IncrementalLoadingCollection<ActiveWorkflowSource, ActiveWorkflow> getActiveWorkflowsList(Action DoneLoadingWorkflows) {
         return new IncrementalLoadingCollection<ActiveWorkflowSource, ActiveWorkflow>(DoneLoadingWorkflows, 2, 8);
      }

      // Get counts of MyTasks and ActiveWorkflows through a query in the VeevaAPI
      public async Task<String> getCounts() {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;
         String ret = "";
         try {
            _totalMyTasks = await VeevaAPI.GetMyTasksCount();
            _totalActiveWorkflows = await VeevaAPI.GetActiveWorkflowsCount();
            ret = "Success";
         }
         catch {
            ret = "Error";
         }
         return ret;
      }
   }
}