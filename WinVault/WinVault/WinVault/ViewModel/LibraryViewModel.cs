﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using WinVault.Common;
using WinVault.Model;
using System.Diagnostics;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Ioc;
using Windows.UI.Popups;

namespace WinVault.ViewModel {
   public class LibraryViewModel : ViewModelBase {

      private readonly IDataService _dataService;
      private readonly INavigationService _navigationService;
      public string PageTitle {
         get { return _pageTitle; }
         set {
            _pageTitle = value;
         }
      }

      static Windows.ApplicationModel.Resources.ResourceLoader loader = new Windows.ApplicationModel.Resources.ResourceLoader();
      private string _pageTitle = loader.GetString("LibraryPageLabel/text");
      public event Action ProgressBarAction;

      //context represents what the user is viewing
      //EX: User is viewing favorite documents, context will be "Favorites"
      private string _context = "";
      public string Context {
         get { return _context; }
         set {
            _context = value;
         }
      }
      private RelayCommand _navigateCommand;

      public LibraryViewModel(IDataService dataService, INavigationService navigationService) {
         _dataService = dataService;
         _navigationService = navigationService;
         Initialize();
      }

      private async Task Initialize() {
         try {

         }
         catch (Exception ex) {
         }
      }

      private void errorDelegate() {
         var msg = new MessageDialog("No internet connection was detected.");
         msg.Title = "No Internet Connection";
         msg.ShowAsync();
         ProgressBarAction();
      }

      private void timeoutDelegate() {
         var msg = new MessageDialog("The connection timed out. Vault might be down.");
         msg.Title = "Connection Timed Out";
         msg.ShowAsync();
         ProgressBarAction();
      }

      public void handleSelectionChanged(Windows.UI.Xaml.Controls.SelectionChangedEventArgs e) {
         //get the document that the user selected
         Document doc = (Document)e.AddedItems[0];
         string[] strArray = new string[2]; //index 0: document id, index 1: document type
         strArray[0] = doc.ID;

         //Binder case: navigate to library page with binder as the type
         if (doc.IsBinder) {
            //tell the viewmodel to get the binder list
            strArray[1] = "binder";
            _pageTitle = doc.DocumentName; //update page title to be name of binder or section
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.LibraryPage), strArray);
         }
         //Section case: navigate to library page with section as the type
         else if (doc.BinderElementMetadata != null && doc.BinderElementMetadata.IsSection) {
            //tell viewmodel to get the section contents list
            strArray[1] = "section";
            _pageTitle = doc.DocumentName; //update page title to be name of binder or section
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.LibraryPage), strArray);
         }
         //Video case: navigate to the video page
         else if (doc.Format.Contains("video")) {
            //tell viewmodel we have a video to load
            strArray[1] = "video";
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.View.DocumentPage), strArray);
         }
         //Renderable document case: navigate to the selected document page
         else {
            //tell viewmodel we have a document to load
            strArray[1] = "document";
            //Note: WinVault.View.DocumentPage is needed due to an inconsistency of namespaces in our project
            SimpleIoc.Default.GetInstance<INavigationService>().Navigate(typeof(WinVault.View.DocumentPage), strArray);
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            _pageTitle = loader.GetString("LibraryPageLabel/text");
         }
      }

      //This method will create a new incremental loading collection class which then does an API call for the documents
      //FLOW: View, onNavigatedTo --> ViewModel getDocumentList --> Model IncrementalLoadingCollection class
      public IncrementalLoadingCollection<DocumentSource, Document> getDocumentList() {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;
         _context = "Document List";
         return new IncrementalLoadingCollection<DocumentSource, Document>(ProgressBarAction);
      }

      //getBinderOrSectionList handles getting the list of documents that are inside a binder or section
      //by calling the model method inside VeevaAPI
      public async Task<ObservableCollection<Document>> getBinderOrSectionList(string docId, string docType) {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;
         _context = "Binder/Section";
         return await VeevaAPI.getBinderOrSectionList(docId, docType);
      }

      //return the document title based off the id
      public string changeTitle(string docId) {
         return VeevaAPI.GetDocumentTitle(docId);
      }

      //This method will create a new incremental loading collection class which then does an API call
      // for "My Documents" only
      public IncrementalLoadingCollection<FilterMyDocumentSource, Document> getMyDocumentList() {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;
         _context = "My Documents";
         return new IncrementalLoadingCollection<FilterMyDocumentSource, Document>(ProgressBarAction);
      }

      //This method will create a new incremental loading collection class which then does an API call
      // for "Recent Documents" only
      public IncrementalLoadingCollection<FilterRecentDocumentSource, Document> getRecentDocumentList() {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;
         _context = "Recent Documents";
         return new IncrementalLoadingCollection<FilterRecentDocumentSource, Document>(ProgressBarAction);
      }

      //This method will create a new incremental loading collection class which then does an API call
      // for "Favorite Documents" only
      public IncrementalLoadingCollection<FilterFavoriteDocumentSource, Document> getFavoriteDocumentList() {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;
         _context = "Favorites";
         return new IncrementalLoadingCollection<FilterFavoriteDocumentSource, Document>(ProgressBarAction);
      }

      //translate view based sort selection to API based sort selection
      public void setSortParameter(string selection) {
         VeevaAPI.setSortParameter(selection);
      }
   }
}