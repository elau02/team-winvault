﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="using:WinVault.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using System.Diagnostics.CodeAnalysis;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using WinVault.Common;
using WinVault.Design;
using WinVault.Model;
using WinVault.ViewModel;

namespace WinVault.ViewModel {
   /// <summary>
   /// This class contains static references to all the view models in the
   /// application and provides an entry point for the bindings.
   /// <para>
   /// See http://www.galasoft.ch/mvvm
   /// </para>
   /// </summary>
   public class ViewModelLocator {
      /// <summary>
      /// Gets the Main property.
      /// </summary>
      [SuppressMessage("Microsoft.Performance",
          "CA1822:MarkMembersAsStatic",
          Justification = "This non-static member is needed for data binding purposes.")]
      public LoginViewModel Login {
         get { return ServiceLocator.Current.GetInstance<LoginViewModel>(); }
      }

      public HomeViewModel Home {
         get { return ServiceLocator.Current.GetInstance<HomeViewModel>(); }
      }

      public LibraryViewModel Library {
         get { return ServiceLocator.Current.GetInstance<LibraryViewModel>(); }
      }

      public MyTasksViewModel MyTasks {
          get { return ServiceLocator.Current.GetInstance<MyTasksViewModel>(); }
      }

      public MyTaskDetailViewModel MyTaskDetail
      {
          get { return ServiceLocator.Current.GetInstance<MyTaskDetailViewModel>(); }
      }

      public DocumentViewModel Document
      {
          get { return ServiceLocator.Current.GetInstance<DocumentViewModel>(); }
      }
      public ActiveWorkflowsViewModel ActiveWorkflows
      {
          get { return ServiceLocator.Current.GetInstance<ActiveWorkflowsViewModel>(); }
      }

      public ActiveWorkflowDetailViewModel ActiveWorkflowDetail
      {
          get { return ServiceLocator.Current.GetInstance<ActiveWorkflowDetailViewModel>(); }
      }

      public SearchViewModel Search {
         get { return ServiceLocator.Current.GetInstance<SearchViewModel>(); }
      }

      public DownloadListViewModel DownloadList
      {
          get { return ServiceLocator.Current.GetInstance<DownloadListViewModel>(); }
      }

      static ViewModelLocator() {
         ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

         if (ViewModelBase.IsInDesignModeStatic) {
            SimpleIoc.Default.Register<IDataService, DesignDataService>();
         }
         else {
            SimpleIoc.Default.Register<IDataService, DataService>();
         }

         SimpleIoc.Default.Register<IDialogService, DialogService>();
         SimpleIoc.Default.Register<INavigationService, NavigationService>();
         SimpleIoc.Default.Register<LoginViewModel>();
         SimpleIoc.Default.Register<HomeViewModel>();
         SimpleIoc.Default.Register<LibraryViewModel>();
         SimpleIoc.Default.Register<MyTasksViewModel>();
         SimpleIoc.Default.Register<DocumentViewModel>();
         SimpleIoc.Default.Register<ActiveWorkflowsViewModel>();
         SimpleIoc.Default.Register<MyTaskDetailViewModel>();
         SimpleIoc.Default.Register<ActiveWorkflowDetailViewModel>();
         SimpleIoc.Default.Register<SearchViewModel>();
         SimpleIoc.Default.Register<DownloadListViewModel>();
      }

      /// <summary>
      /// Cleans up all the resources.
      /// </summary>
      public static void Cleanup() {
      }
   }
}