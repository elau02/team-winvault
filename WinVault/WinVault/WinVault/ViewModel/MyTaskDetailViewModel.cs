﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using WinVault.Common;
using WinVault.Model;
using WinVault.ViewModel;
using System.Collections.ObjectModel;
using Windows.Storage;

namespace WinVault.ViewModel {
   public class MyTaskDetailViewModel : ViewModelBase {

      private readonly IDataService _dataService;
      private readonly INavigationService _navigationService;

      private RelayCommand _navigateCommand;

      private MyTask _myTask;
      public MyTask MyTask {
         get { return _myTask; }
         set { _myTask = value; }
      }

      public MyTaskDetailViewModel(IDataService dataService, INavigationService navigationService) {
         _dataService = dataService;
         _navigationService = navigationService;
         Initialize();
      }

      private async Task Initialize() {
         try {
         }
         catch (Exception ex) {
         }
      }

      public async Task<String> getMyTask(string taskID) {
         _myTask = await VeevaAPI.GetMyTask(taskID);
         System.Diagnostics.Debug.WriteLine("Task ID is: " + MyTask.TaskID);
         if (_myTask != null) {
            return _myTask.TaskName;
         }
         return "";
      }

      public async Task<StorageFile> getVideo(string docId) {
         return await VeevaAPI.GetVideo(docId, null, null);
      }

      public async Task<string> getDocumentFormat(string docId) {
         Document doc = await VeevaAPI.GetDocumentById(docId);
         return doc.Format;
      }

      //getDocuments is called from the library page when the user selects a document to view
      public async Task<ObservableCollection<Image>> getDocument(string docId) {
         System.Diagnostics.Debug.WriteLine("render");
         return await VeevaAPI.renderDocument(docId, null, null);
      }
   }
}
