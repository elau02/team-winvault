﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Controls;
using WinVault.Common;
using WinVault.Model;

namespace WinVault.ViewModel {
   public class DocumentViewModel : ViewModelBase {

      private readonly IDataService _dataService;
      private readonly INavigationService _navigationService;

      private RelayCommand _navigateCommand;

      private Document _document;
      public Document Document {
         get { return _document; }
         set { _document = value; }
      }

      /// <summary>
      /// Initializes a new instance of the LibraryViewModel class.
      /// </summary>
      public DocumentViewModel(IDataService dataService, INavigationService navigationService) {
         _dataService = dataService;
         _navigationService = navigationService;
         Initialize();
      }

      private async Task Initialize() {
         try {
         }
         catch (Exception ex) {
         }
      }

      public async void SetId(string docId) {
         Document = await VeevaAPI.GetDocumentProperties(docId);
      }

      //getDocuments is called from the library page when the user selects a document to view
      public async Task<ObservableCollection<Image>> getDocument(string docId, string major, string minor) {
         return await VeevaAPI.renderDocument(docId, major, minor);
      }

      //downloadDocument is called from the view when a user clicks on the download button 
      public async Task<byte[]> downloadDocument() {
         return await VeevaAPI.DownloadDocument(_document.ID);
      }

      public async Task<StorageFile> getVideo(string docId, string major, string minor) {
         return await VeevaAPI.GetVideo(docId, major, minor);
      }

      public async Task<ActiveWorkflow> getActiveWorkflow(string docID) {
         return await VeevaAPI.GetActiveWorkflowByDocumentID(docID);
      }

      public async Task<string> GetGroupName(string groupId) {
         return await VeevaAPI.GetGroupName(groupId);
      }

      public async Task<string> GetUser(string userId) {
         VaultUser user = await VeevaAPI.GetUser(userId);
         return user.FullName;
      }

      public async Task<ObservableCollection<string>> GetDocumentVersions(string id) {
         ObservableCollection<string> docs = await VeevaAPI.GetDocumentVersions(id);
         return docs;
      }

      public async Task<List<Document>> GetSupportingDocuments(string docId, string major, string minor) {

         List<Document> docs = await VeevaAPI.GetSupportingDocuments(docId, major, minor);
         return docs;
      }
   }
}
