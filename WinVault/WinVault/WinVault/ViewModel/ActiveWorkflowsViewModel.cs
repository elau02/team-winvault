﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using WinVault.Common;
using WinVault.Model;
using System.Diagnostics;
using Windows.UI.Popups;

namespace WinVault.ViewModel {
   /// <summary>
   /// This class contains properties that the main View can data bind to.
   /// <para>
   /// See http://www.galasoft.ch/mvvm
   /// </para>
   /// </summary>
   public class ActiveWorkflowsViewModel : ViewModelBase {

      private readonly IDataService _dataService;
      private readonly INavigationService _navigationService;

      private RelayCommand _navigateCommand;

      /// <summary>
      /// Initializes a new instance of the LibraryViewModel class.
      /// </summary>
      public ActiveWorkflowsViewModel(IDataService dataService, INavigationService navigationService) {
         _dataService = dataService;
         _navigationService = navigationService;
         Initialize();
      }

      private async Task Initialize() {
         try {

         }
         catch (Exception ex) {

         }
      }

      public delegate void VoidDelegate();
      public VoidDelegate AWProgressBar;

      private void errorDelegate() {
         var msg = new MessageDialog("No internet connection was detected.");
         msg.Title = "No Internet Connection";
         msg.ShowAsync();
         AWProgressBar();
      }

      private void timeoutDelegate() {
         var msg = new MessageDialog("The connection timed out. Vault might be down.");
         msg.Title = "Connection Timed Out";
         msg.ShowAsync();
         AWProgressBar();
      }
      //This method will create a new incremental loading collection class which then does an API call for the documents
      //FLOW: View, onNavigatedTo --> ViewModel getActiveWorkflowsList --> Model IncrementalLoadingCollection class
      public IncrementalLoadingCollection<ActiveWorkflowSource, ActiveWorkflow> getActiveWorkflowsList(Action action) {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;
         return new IncrementalLoadingCollection<ActiveWorkflowSource, ActiveWorkflow>(action);
      }
   }
}