﻿using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using WinVault.Common;
using WinVault.Model;
using System.Diagnostics;
using Windows.UI.Popups;

namespace WinVault.ViewModel {
   public class SearchViewModel : ViewModelBase {

      private readonly IDataService _dataService;
      private readonly INavigationService _navigationService;

      private RelayCommand _navigateCommand;
      public SearchViewModel(IDataService dataService, INavigationService navigationService) {
         _dataService = dataService;
         _navigationService = navigationService;
         Initialize();
      }

      private async Task Initialize() {
         try {
            
         }
         catch (Exception ex) {
         }
      }

      private static void errorDelegate() {
         var msg = new MessageDialog("No internet connection was detected.");
         msg.Title = "No Internet Connection";
         msg.ShowAsync();
      }

      private static void timeoutDelegate() {
         var msg = new MessageDialog("The connection timed out. Vault might be down.");
         msg.Title = "Connection Timed Out";
         msg.ShowAsync();
      }

      //This method will create a new incremental loading collection class which then does an API call for the documents
      //FLOW: View, onNavigatedTo --> ViewModel getSearchResults --> Model IncrementalLoadingCollection class
      public static IncrementalLoadingCollection<SearchSource, Document> getSearchResults(string searchTerm, Action action) {
         VeevaAPI.ErrorHandlingDelegates[0] = errorDelegate;
         VeevaAPI.ErrorHandlingDelegates[1] = timeoutDelegate;

         VeevaAPI.SearchTerm = searchTerm;
         return new IncrementalLoadingCollection<SearchSource, Document>(action);
      }
   }
}
