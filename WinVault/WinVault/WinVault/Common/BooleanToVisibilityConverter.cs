﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Globalization;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml;

namespace WinVault.Common {
   class BooleanToVisibilityConverter : IValueConverter {
      public object Convert(object value, Type targetType, object parameter, string language) {
         if (!(value is bool))
            throw new ArgumentException("value is not type of boolean");

         return (bool)value ? Visibility.Visible : Visibility.Collapsed;
      }

      public object ConvertBack(object value, Type targetType, object parameter, string language) {
         if (!(value is Visibility))
            throw new ArgumentException("value is not type of Visibility");

         return (Visibility)value == Visibility.Visible;
      }
   }
}
