﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Globalization;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml;

namespace WinVault.Common {
   class NullStringToVisibilityConverter : IValueConverter {
      public object Convert(object value, Type targetType, object parameter, string language) {
         if (value != null) {
            return Visibility.Visible;
         }
         return Visibility.Collapsed;
      }

      public object ConvertBack(object value, Type targetType, object parameter, string language) {
         if (!(value is string))
            throw new ArgumentException("value is not type of string");
         if ((Visibility)value == Visibility.Visible)
            return (string)value;
         return null;
      }
   }
}
