﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Globalization;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml;

namespace WinVault.Common {
   class FileTypeToSymbolIconConverter : IValueConverter {
      public object Convert(object value, Type targetType, object parameter, string language) {
         if (!(value is string))
            throw new ArgumentException("value is not type of string");
         if (((string)value).Contains("mp4"))
            return Symbol.Play;
         if (((string)value).Contains("png"))
            return Symbol.Pictures;
         return Symbol.Document;
      }

      public object ConvertBack(object value, Type targetType, object parameter, string language) {
         if (!(value is Symbol))
            throw new ArgumentException("value is not type of string");
         if ((Symbol)value == Symbol.Play)
            return ".mp4";
         if ((Symbol)value == Symbol.Pictures)
            return ".png";
         return ".pdf";
      }
   }
}
