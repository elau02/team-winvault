﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using WinVault.Model;
using WinVault.ViewModel;
using WinVault.Common;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Windows.Storage;

namespace Tests {
   [TestClass]
   public class VeevaApiTest {

      //author - Talha
      [ClassInitialize()]
      public async static Task ClassInit(TestContext context) {
         VeevaAPI api = new VeevaAPI();
         Login user = new Login("tsaifi@calpoly-capstone.com", "Testuser1");
         Task tsk = VeevaAPI.Login(user, null, null, null, null);
         tsk.Wait();
      }
      
      //author - Ebele
      [TestMethod]
      public void TestLogin() {
         Login login = new Login("tsaifi@calpoly-capstone.com", "Testuser1");
         Task setuser = VeevaAPI.SetUserInformation(login, null, null);
         setuser.Wait();
         User user = User.Instance;
         Assert.IsNotNull(user.SessionId);
         Assert.AreEqual(3, user.Vaults.Count);
      }

      //author - Ebele
      [TestMethod]
      public void TestSearchVault() {
         VeevaAPI.SearchTerm = "srs";
         Task<ObservableCollection<Document>> search = VeevaAPI.SearchVault(0, 5);
         search.Wait();

         ObservableCollection<Document> results = search.Result;
         Assert.AreNotEqual(0, results.Count);

         VeevaAPI.SearchTerm = "gundam";
         search = VeevaAPI.SearchVault(0, 5);
         search.Wait();

         results = search.Result;
         Assert.AreEqual(0, results.Count);
      }

      //author - Ebele
      [TestMethod]
      public void TestUpdateDocumentProperty() {
         Task<HttpResponseMessage> response = VeevaAPI.UpdateDocumentProperty("5", "name__v", "Edited By Test User");
         response.Wait();

         HttpResponseMessage message = response.Result;
         dynamic res = JObject.Parse(message.Content.ReadAsStringAsync().Result);
         Assert.AreEqual("SUCCESS", (string)res.responseStatus);
      }

      // author - Ebele
      [TestMethod]
      public void TestGetRecentDocuments() {
         Task<ObservableCollection<Document>> result = VeevaAPI.getRecentDocuments(0, 5);
         result.Wait();

         ObservableCollection<Document> docs = result.Result;
         Assert.AreNotEqual(0, docs.Count);
      }

      //author - Talha
      [TestMethod]
      public void TestLibrarySize() {
         var num = VeevaAPI.getLibrarySize().Result;
         Assert.AreEqual(num, VeevaAPI.LibrarySize);
      }

      //author - Talha
      [TestMethod]
      public void TestGetUser() {
         string talha = "59465";
         VaultUser user = VeevaAPI.GetUser(talha).Result;

         Assert.AreEqual("Talha Saifi", user.FullName);
         Assert.AreEqual("Talha", user.FirstName);
         Assert.AreEqual("Saifi", user.LastName);
         Assert.AreEqual("tsaifi@calpoly-capstone.com", user.Username);
      }

      //author - Talha
      [TestMethod]
      public void TestGetActiveWorkflowsCount() {
         //this user has 0 workflows
         Assert.AreNotEqual(0, VeevaAPI.GetActiveWorkflowsCount().Result);
      }

      //author - Talha
      [TestMethod]
      public void TestGetMyTasksCount() {
         //this user has 0 tasks
         Assert.AreNotEqual(0, VeevaAPI.GetMyTasksCount().Result);
      }

      //author - Daniel
      //This test will retrieve one document from the API and test that its metadata is correct
      //Tests VeevaAPI.obtainMetadata(), Document class, VaultUser class
      [TestMethod]
      public void TestGetDocument() {
         Document doc = VeevaAPI.GetDocumentById("289").Result;
         //missing LastModifiedBy, ThumbnailImage, Properties
         Assert.AreEqual("zzzz", doc.DocumentName);
         Assert.AreEqual("Title Test", doc.Title);
         Assert.AreEqual("veevaPromXR\n", doc.ProductName);
         Assert.AreEqual("Base Document", doc.DocumentType);
         Assert.AreEqual("289", doc.ID);
         Assert.IsFalse(doc.IsBinder);
         Assert.AreEqual("VV-00289", doc.Number);
         Assert.AreEqual("application/pdf", doc.Format);
         Assert.AreEqual(59465, doc.CreatedBy.Id);
         Assert.AreEqual("Talha", doc.CreatedBy.FirstName);
         Assert.AreEqual("0.1", doc.Version);
         Assert.AreEqual("General Lifecycle", doc.Lifecycle);
         Assert.AreEqual("IN REVIEW", doc.Status);
         Assert.AreEqual(59465, doc.Owner.Id);
         Assert.AreEqual(null, doc.BinderElementMetadata);
      }

      //author - Daniel
      //This test will retrieve a binder and ensure the contents of the binder are correct
      //Test getBinderOrSectionList
      [TestMethod]
      public void TestGetBinder() {
         Task<ObservableCollection<Document>> task = VeevaAPI.getBinderOrSectionList("308", "binder");
         task.Wait();
         System.Collections.ObjectModel.ObservableCollection<Document> binderList = task.Result;
         //test to see that the requirements document is inside the binder
         Document requirementDoc = (Document)binderList.Where(x => x.DocumentName == "Requirement Doc").FirstOrDefault();
         //test some known elements of the requirements document
         Assert.AreEqual("Requirement Doc", requirementDoc.DocumentName);
         Assert.AreEqual("", requirementDoc.Title);
         Assert.AreEqual("veevaProm\nveevaPromXR\nwonderDrug\ncholecap\n", requirementDoc.ProductName);
         Assert.AreEqual("Base Document", requirementDoc.DocumentType);
         Assert.AreEqual("108", requirementDoc.ID);
         Assert.IsFalse(requirementDoc.IsBinder);
      }

      // Author: Elaine
      // The test will retrieve from the API 5 documents from
      // "My Documents" (filtered documents) and check if
      // the documents returned were actually created or updated
      // by the current user.
      [TestMethod]
      public void TestGetMyDocuments()
      {
          // Get "My Documents" from VeevaAPI
          Task<ObservableCollection<Document>> result = VeevaAPI.getMyDocuments(0, 5);
          result.Wait();
          ObservableCollection<Document> docs = result.Result;

          // Check if each document was created or last modified by the current user
          foreach (Document doc in docs) {
              Assert.AreEqual(doc.CreatedBy.Id.ToString(), User.Instance.UserId.ToString());
              //Assert.AreEqual(doc.LastModifiedBy.Id.ToString(), User.Instance.UserId.ToString());
          }
      }

      // Author: Elaine
      // The test will pass in a known Document ID string
      // and check if the Downloads folder contains
      // the downloaded file representing the known document.
      [TestMethod]
      public async void TestDownloadDocument()
      {
          // Pass in a known document ID
          await VeevaAPI.DownloadDocument("305");

          // Check if the file exists in Downloads folder
          var sourceFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
          sourceFolder = await sourceFolder.GetFolderAsync("Downloads");
          var files = await sourceFolder.GetFilesAsync();
          string requiredFile = "zzzzzzzzzzz1.jpg";
          Assert.IsTrue((files.Any(f => f.Name == requiredFile)));

      }
   
      [TestMethod]
      public void TestGetFavorites() {
         Task<ObservableCollection<Document>> result = VeevaAPI.getFavoriteDocuments(0, 5);
         result.Wait();

         ObservableCollection<Document> docs = result.Result;
         Assert.AreEqual(5, docs.Count); // there's actually 6 but it will retrieve 5 b/c of incremental loading
      }

      [TestMethod]
      public void TestGetDocProperties() {
         Task<Document> result = VeevaAPI.GetDocumentProperties("323"); // get srs-v1
         result.Wait();

         Document srs = result.Result;
         Assert.AreEqual("srs-v1", srs.DocumentName);
         Assert.AreEqual("Software Requirements Specification", srs.Title);
      }

      [TestMethod]
      public void TestSetDocTypes() {
         Task result = VeevaAPI.SetDocumentTypes();
         result.Wait();

         List<string> types = VeevaAPI.GetDocumentTypes();
         Assert.AreEqual(4, types.Count);
      }

      [TestMethod]
      public void TestGetDocVersions() {
         Task<ObservableCollection<string>> result = VeevaAPI.GetDocumentVersions("323");
         result.Wait();

         Assert.AreEqual(3, result.Result.Count);

      }
   
   }
}
